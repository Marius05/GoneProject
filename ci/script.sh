#!/bin/bash

curl -sL https://deb.nodesource.com/setup_14.x | bash -

apt-get update --fix-missing

apt-get install -y git \
  zip \
  libzip-dev \
  nodejs \
  libcurl3-dev \
  curl \
  libpng-dev \
  libfreetype6-dev \
  libjpeg-dev \
  fontconfig \
  xfonts-75dpi \
  xfonts-base \
  libxrender1

docker-php-ext-install mysqli pdo_mysql zip
docker-php-ext-configure gd \
  && docker-php-ext-install gd \
  && docker-php-source delete \
  && rm -r /var/lib/apt/lists/*

# Install Xdebug
pecl install xdebug
docker-php-ext-enable xdebug-

curl -sSk https://getcomposer.org/installer | php -- --disable-tls && mv composer.phar /usr/local/bin/composer

cd symfony
mkdir -p files/zip
npm i
