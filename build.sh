#!/bin/bash

PROD_PATH=./docker/prod/symfony

echo "COPY symfony folder to $PROD_PATH"
cp -Rf ./symfony $PROD_PATH

echo "REMOVE Git, Cache, Vendor and Idea folder"
rm -Rf $PROD_PATH/.git
rm -Rf $PROD_PATH/.gitignore
rm -Rf $PROD_PATH/phpstan.neon
rm -Rf $PROD_PATH/phpunit.xml.dist
rm -Rf $PROD_PATH/node_modules
rm -Rf $PROD_PATH/nightwatch
rm -Rf $PROD_PATH/nightwatch.json
rm -Rf $PROD_PATH/config/packages/dev
rm -Rf $PROD_PATH/config/packages/test
rm -Rf $PROD_PATH/config/routes/dev
rm -Rf $PROD_PATH/var/log/*
rm -Rf $PROD_PATH/var/cache/*
rm -Rf $PROD_PATH/.idea
rm -Rf $PROD_PATH/vendor
rm -Rf $PROD_PATH/test
rm -Rf $PROD_PATH/bin/phpunit

echo "BUILD IMAGE"

#docker build ./docker/prod --no-cache -t ${DOCKER_REGISTRY_ENDPOINT}/daily/app:latest
docker build ./docker/prod --no-cache -t tyllt:latest

#echo "LOG TO THE REGISTRY ${DOCKER_REGISTRY_ENDPOINT} WITH ${PORTUS_USERNAME}"
#docker login ${DOCKER_REGISTRY_ENDPOINT} -u ${PORTUS_USERNAME} -p ${PORTUS_PASSWORD}

#echo "PUSH ${DOCKER_REGISTRY_ENDPOINT}/daily/app:latest"
#docker push ${DOCKER_REGISTRY_ENDPOINT}/daily/app:latest

#echo "LOGOUT ${DOCKER_REGISTRY_ENDPOINT}"
#docker logout ${DOCKER_REGISTRY_ENDPOINT}

echo "REMOVE Temp $PROD_PATH"
rm -Rf $PROD_PATH
