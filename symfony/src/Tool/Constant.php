<?php

declare(strict_types=1);

namespace App\Tool;

class Constant
{
    /**
     * @var string
     */
    public const DATE_FORMAT = 'dd/MM/yyyy';

    /**
     * @var string
     */
    public const DATE_APP_FORMAT = 'ddMMyyyy';

    /**
     * @var string
     */
    public const DATE_HOUR_FORMAT = 'dd/MM/yyyy HH:mm';
}
