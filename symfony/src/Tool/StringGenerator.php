<?php

declare(strict_types=1);

namespace App\Tool;

/**
 * Class StringGenerator.
 */
class StringGenerator
{
    protected const UPPERCASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    protected const LOWERCASE = 'abcdefghijklmnopqrstuvwxyz';

    protected const NUMBERS = '0123456789';

    public static function generate(int $length = 20, bool $lowercase = true, bool $uppercase = true, bool $number = true): string
    {
        $characters = $lowercase ? self::LOWERCASE : '';
        $characters .= $uppercase ? self::UPPERCASE : '';
        $characters .= $number ? self::NUMBERS : '';

        $charactersLength = \strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; ++$i) {
            try {
                $randomString .= $characters[random_int(0, $charactersLength - 1)];
            } catch (\Throwable $exception) {
            }
        }

        return $randomString;
    }
}
