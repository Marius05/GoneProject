<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Company;
use OpenApi\Annotations as OA;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyEditType.
 *
 * @OA\Schema(
 *     title="CompanyType",
 *     @OA\Property(property="name", type="string", required={"true"}),
 *     @OA\Property(property="legalForm", type="string", required={"true"}),
 *     @OA\Property(property="siret", type="string", required={"true"}),
 *     @OA\Property(property="activity", type="string", required={"true"}),
 *
 *     @OA\Property(property="street", type="string", required={"false"}),
 *     @OA\Property(property="postalCode", type="string", required={"false"}),
 *     @OA\Property(property="city", type="string", required={"false"}),
 *     @OA\Property(property="country", type="string", required={"false"})
 * )
 */
class CompanyEditType extends CompanyType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->remove('type')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
