<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Company;
use OpenApi\Annotations as OA;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class CompanyType.
 *
 * @OA\Schema(
 *     title="CompanyType",
 *     @OA\Property(property="name", type="string", required={"true"}),
 *     @OA\Property(property="type", type="string", required={"true"}),
 *     @OA\Property(property="legalForm", type="string", required={"true"}),
 *     @OA\Property(property="siret", type="string", required={"true"}),
 *     @OA\Property(property="activity", type="string", required={"true"}),
 *
 *     @OA\Property(property="street", type="string", required={"false"}),
 *     @OA\Property(property="postalCode", type="string", required={"false"}),
 *     @OA\Property(property="city", type="string", required={"false"}),
 *     @OA\Property(property="country", type="string", required={"false"})
 * )
 */
class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('legalForm', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('siret', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('activity', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('street', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('postalCode', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('city', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('country', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
