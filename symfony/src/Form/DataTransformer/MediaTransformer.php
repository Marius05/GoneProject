<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use App\Entity\Media;
use App\Manager\MediaManager;
use Symfony\Component\Form\DataTransformerInterface;

class MediaTransformer implements DataTransformerInterface
{
    protected MediaManager $manager;

    public function __construct(MediaManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param ?Media $value
     */
    public function transform($value): ?string
    {
        if (!$value) {
            return null;
        }

        return $value->getId();
    }

    /**
     * @param string $value
     */
    public function reverseTransform($value): ?Media
    {
        if (!$value) {
            return null;
        }

        return $this->manager->fetch($value);
    }
}
