<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use OpenApi\Annotations as OA;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class RequestPasswordType.
 *
 * @OA\Schema(
 *     title="RequestPasswordType",
 *     @OA\Property(
 *         property="plainPassword",
 *         type="object",
 *         required={"true"},
 *         @OA\Schema(
 *             @OA\Property(property="first", type="string"),
 *             @OA\Property(property="second", type="string"),
 *         )
 *     ),
 * )
 */
class RequestPasswordType extends SignType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->remove('email')
            ->add('password', TextType::class, [
                'mapped' => false,
                'constraints' => [
                    new NotNull([
                        'groups' => 'check',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
            'validation_groups' => [],
        ]);
    }
}
