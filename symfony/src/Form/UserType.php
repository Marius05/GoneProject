<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Form\DataTransformer\MediaTransformer;
use OpenApi\Annotations as OA;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UserType.
 *
 * @OA\Schema(
 *     title="UserType",
 *     @OA\Property(property="type", type="string", required={"true"}),
 *     @OA\Property(property="lastName", type="string", required={"true"}),
 *     @OA\Property(property="firstName", type="string", required={"true"}),
 *     @OA\Property(property="phone", type="string", required={"true"}),
 * )
 */
class UserType extends AbstractType
{
    public function __construct(
        private MediaTransformer $transformer,
    )
    { }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'user' => 'user',
                    'owner' => 'owner',
                ],
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('lastName', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('firstName', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('phone', TextType::class)
            ->add('description', TextType::class)
            ->add('avatar', TextType::class)
        ;

        $builder->get('avatar')
            ->addModelTransformer($this->transformer)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }
}
