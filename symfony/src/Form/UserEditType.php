<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Form\DataTransformer\MediaTransformer;
use OpenApi\Annotations as OA;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserEditType.
 *
 * @OA\Schema(
 *     title="UserEditType",
 *     @OA\Property(property="lastName", type="string", required={"true"}),
 *     @OA\Property(property="firstName", type="string", required={"true"}),
 *     @OA\Property(property="phone", type="string", required={"true"}),
 *     @OA\Property(property="description", type="string", required={"true"}),
 * )
 */
class UserEditType extends UserType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->remove('type')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'allow_extra_fields' => true,
            'csrf_protection' => false,
        ]);
    }
}
