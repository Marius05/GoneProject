<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Project;
use OpenApi\Annotations as OA;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class UserEditType.
 *
 * @OA\Schema(
 *     title="UserEditType",
 *     @OA\Property(property="name", type="string", required={"true"}),
 *     @OA\Property(property="resume", type="string", required={"true"}),
 *     @OA\Property(property="tags", type="string", required={"false"}),
 *     @OA\Property(property="description", type="string", required={"false"}),
 * )
 */
class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('resume', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('tags', TextType::class)
            ->add('description', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
            'allow_extra_fields' => true,
            'csrf_protection' => false,
        ]);
    }
}
