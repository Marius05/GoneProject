<?php

declare(strict_types=1);

namespace App\Listener;

use App\Entity\Media;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class FileListener
{
    protected RouterInterface $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function postLoad(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Media) {
            return;
        }

        $entity->setPath(
            $this->router->generate(
                'media_display',
                ['uid' => $entity->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL
            )
        );
    }
}
