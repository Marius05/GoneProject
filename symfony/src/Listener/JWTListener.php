<?php

declare(strict_types=1);

namespace App\Listener;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class JWTListener
{
    protected ?User $user = null;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        if ($token = $tokenStorage->getToken()) {
            /** @var ?User $user */
            $user = $token->getUser();
            $this->user = $user;
        }
    }

    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        if (!$this->user) {
            return;
        }

        $payload = $event->getData();
        $payload['firstName'] = $this->user->getFirstName();
        $payload['verified'] = true;

        $event->setData($payload);
    }
}
