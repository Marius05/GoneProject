<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\User;
use App\Manager\UserManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class UserFixtures extends Fixture
{
    public const ADMIN_EMAIL = 'admin@gone.fr';

    public const OWNER_EMAIL = 'developer@gone.fr';

    public const USER_EMAIL = 'user@gone.fr';

    protected Generator $faker;

    protected UserManager $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        $company = (new Company())
            ->setName('Médecin sans frontière')
            ->setActivity('Social')
            ->setDescription($this->faker->text)
            ->setEmail($this->faker->email)
            ->setWebsite($this->faker->url)
        ;

        $owner = (new User())
            ->addRole('ROLE_OWNER')
            ->setType(User::TYPE_OWNER)
            ->setFirstName($this->faker->firstName)
            ->setLastName($this->faker->lastName)
            ->setPhone($this->faker->e164PhoneNumber)
            ->setEmail(self::OWNER_EMAIL)
            ->setCompany($company)
        ;

        $admin = (new User())
            ->addRole('ROLE_ADMIN')
            ->setType(User::TYPE_ADMIN)
            ->setFirstName($this->faker->firstName)
            ->setLastName($this->faker->lastName)
            ->setEmail(self::ADMIN_EMAIL)
        ;

        $user = (new User())
            ->setFirstName($this->faker->firstName)
            ->setLastName($this->faker->lastName)
            ->setPhone($this->faker->e164PhoneNumber)
            ->setEmail(self::USER_EMAIL)
            ->addRole('ROLE_USER')
            ->setType(User::TYPE_USER)
            ->setDescription('Je suis un développeur')
            ->setJob('Développeur backend')
        ;

        $this->userManager->upgradePassword($owner, 'developer');
        $this->userManager->upgradePassword($admin, 'admin');
        $this->userManager->upgradePassword($user, 'user');

        $manager->persist($company);
        $manager->persist($owner);
        $manager->persist($admin);
        $manager->persist($user);
        $manager->flush();
    }
}
