<?php

declare(strict_types=1);

namespace App\Manager;

use App\Entity\User;
use App\Form\AdminType;
use App\Form\AdminUpdateType;
use App\Form\RequestPasswordType;
use App\Form\SignType;
use App\Form\UserEditType;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\MailService;
use App\Tool\StringGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserManager extends AbstractManager implements PasswordUpgraderInterface
{
    protected Generator $faker;

    public function __construct(
        protected EntityManagerInterface $manager,
        protected FormFactoryInterface $formFactory,
        protected TokenStorageInterface $storage,
        protected UserRepository $repository,
        protected UserPasswordEncoderInterface $encoder,
        protected MediaManager $mediaManager,
        protected CompanyManager $companyManager,
        protected EventDispatcherInterface $dispatcher,
        protected MailService $mailService,
        protected string $frontDNS
    ) {
        parent::__construct($manager, $formFactory, $storage);
        $this->faker = Factory::create();
    }

    /**
     * @return User[]
     */
    public function fetchAll(
        int $page = 1,
        int $items = 6,
        ?string $sortBy = null,
        ?string $sortDirection = null,
        ?string $status = null,
        ?string $search = null
    ): array {
        return $this->repository
            ->fetchAll(
                $page,
                $items,
                $sortBy,
                $sortDirection,
                $status,
                $search
            )
            ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     *
     * @return mixed[]
     */
    public function count(
        ?string $sortBy = null,
        ?string $sortDirection = null,
        ?string $status = null,
        ?string $search = null
    ): array {
        return $this->repository
            ->length(
                $sortBy,
                $sortDirection,
                $status,
                $search
            )
            ;
    }

    public function fetch(int $id, bool $wallet = false): User
    {
        try {
            $user = $this->repository->fetch($id);
        } catch (NonUniqueResultException $exception) {
            throw new NotFoundHttpException();
        }

        if (!$user) {
            throw new NotFoundHttpException();
        }

        // Si l'utilisateur est un propriétaire et que son compte bancaire n'est pas renseigné ou pas encore validé
        // On load les infos de son wallet et on check la balance
        if ($wallet && \in_array('ROLE_OWNER', $user->getRoles(), true)) {
            try {
                if (!$user->getBankAccount() || BankAccount::VALIDATED !== $user->getBankAccount()->getState()) {
                    $wallet = $this->payment->getWallet($user->getWalletExternalId());
                    $user->setBalance($wallet['balance_available']);
                }
            } catch (\Throwable $e) {
            }
        }

        return $user;
    }

    public function fetchByEmail(string $email, ?bool $anon = null): User
    {
        try {
            $user = $this->repository->fetchByEmail($email, $anon);
        } catch (NonUniqueResultException $exception) {
            throw new NotFoundHttpException();
        }

        if (!$user) {
            throw new NotFoundHttpException();
        }

        return $user;
    }

    public function fetchByRequestPassword(string $token): User
    {
        try {
            $user = $this->repository->fetchByToken($token);
        } catch (NonUniqueResultException $exception) {
            throw new NotFoundHttpException();
        }

        if (!$user) {
            throw new NotFoundHttpException();
        }

        return $user;
    }

    /**
     * @param array<mixed> $data
     *
     * @return FormInterface|User
     * @throws Exception
     */
    public function sign(array $data)
    {
        try {
            $this->fetchByEmail($data['email']);
            throw new BadRequestHttpException('This user already exist', code:400);
        } catch (NotFoundHttpException $exception) {
            $user = new User();
        }

        $form = $this->formFactory->create(SignType::class, $user);
        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var User $user */
        $user = $form->getData();
        $this->upgradePassword($user, $user->getPlainPassword());

        $this->manager->persist($user);
        $this->manager->flush();

        $this->updateSign($data, $user);

        return $user;
    }

    /**
     * @param array<mixed> $data
     *
     * @throws Exception
     *
     * @return FormInterface|User
     */
    public function updateSign(array $data, $user)
    {
        $form = $this->formFactory->create(UserType::class, $user);
        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var User $user */
        $user = $form->getData();

        if (User::TYPE_OWNER === $user->getType()) {
            $user->addRole('ROLE_OWNER');
        } else {
            $user->addRole('ROLE_USER');
        }

        $this->manager->flush();

        return $user;
    }

    /**
     * @param array<mixed> $data
     *
     * @throws Exception
     *
     * @return FormInterface|User
     */
    public function update(array $data, ?int $id = null)
    {
        $user = $id ? $this->fetch($id) : $this->user;

        $form = $this->formFactory->create(UserEditType::class, $user);
        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var User $user */
        $user = $form->getData();

        $this->manager->flush();

        return $user;
    }

    public function requestPassword(string $email): string
    {
        $user = $this->fetchByEmail($email, false);
        $user->setRequestPassword(
            StringGenerator::generate(200)
        )->setRequestPasswordAt(new \DateTime());

        $this->manager->flush();

        $mail = $this->mailService->prepare(
            $email,
            'Demande de changement de mot de passe',
            'Mail/password.mjml.twig',
            [
                'url' => sprintf(
                    '%s/auth/request-password/%s',
                    $this->frontDNS,
                    $user->getRequestPassword()
                ),
            ]
        );

        $this->mailService->send($mail);

        return $user->getRequestPassword();
    }

    /**
     * @param mixed[] $data
     *
     * @return FormInterface|User
     */
    public function changePasswordToken(array $data, string $token)
    {
        return $this->changePassword($data, $token);
    }

    /**
     * @param mixed[] $data
     *
     * @return FormInterface|User
     */
    public function changePasswordWithCheck(array $data)
    {
        return $this->changePassword($data, null, true);
    }

    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $encoded = $this->encoder->encodePassword($user, $newEncodedPassword);
        $user->setPassword($encoded);
    }

    /**
     * @param array<mixed> $data
     *
     * @throws Exception
     *
     * @return FormInterface|User
     */
    public function createAdmin(array $data)
    {
        $user = new User();

        $form = $this->formFactory->create(AdminType::class, $user);

        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var User $user */
        $user = $form->getData();
        $user->setType(User::TYPE_ADMIN);
        $user->addRole('ROLE_ADMIN');

        $this->upgradePassword($user, $user->getPlainPassword());

        $this->manager->persist($user);
        $this->manager->flush();

        return $user;
    }

    /**
     * @param array<mixed> $data
     *
     * @throws Exception
     *
     * @return FormInterface|User
     */
    public function updateAdmin(array $data, ?int $id = null)
    {
        $user = $id ? $this->fetch($id) : $this->user;

        $form = $this->formFactory->create(AdminUpdateType::class, $user);

        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var User $user */
        $user = $form->getData();

        $this->manager->flush();

        return $user;
    }

    public function delete(int $id): void
    {
        $user = $this->fetch($id);
        //Impossible de supprimer l'utilisateur actuel
        if ($this->user->getId() === $user->getId()) {
            throw new BadRequestHttpException('Impossible de supprimer votre compte');
        }
        $this->manager->remove($user);
        $this->manager->flush();
    }

    public function anonymize(int $id): void
    {
        $user = $this->fetch($id);
        $this->manager->beginTransaction();

        try {
            //Impossible de supprimer l'utilisateur actuel
            if ($this->user->getId() === $user->getId()) {
                throw new BadRequestHttpException('Impossible de supprimer votre compte');
            }

            $user->setFirstName('Anon.')
                ->setLastName('Anonyme')
                ->setEmail(md5((string) time()).'@deleted.com')
                ->setPhone($this->faker->e164PhoneNumber)
                ->setDeletedAt(new \DateTime())
            ;

            $picture = $user->getAvatar();
            $user->setAvatar(null);
            if ($picture) {
                $this->manager->remove($picture);
            }

            $this->manager->flush();
            // Fin de transaction
            $this->manager->commit();
        } catch (\Exception $e) {
            // Erreur de transaction -> rollback
            $this->manager->rollback();

            throw new BadRequestHttpException('Erreur lors de la suppression');
        }
    }

    /**
     * @param mixed[] $data
     *
     * @return FormInterface|User
     */
    private function changePassword(array $data, ?string $token = null, bool $check = false)
    {
        $user = $token ? $this->fetchByRequestPassword($token) : $this->user;
        $form = $this->formFactory->create(RequestPasswordType::class, $user);
        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var User $user */
        $user = $form->getData();

        if ($check) {
            if (!$this->encoder->isPasswordValid($user, $form->get('password')->getData())) {
                throw new BadRequestHttpException('Password not good');
            }
        }

        $user->setRequestPassword(null)
            ->setRequestPasswordAt(null)
        ;
        $this->upgradePassword($user, $user->getPlainPassword());

        $this->manager->flush();

        return $user;
    }
}
