<?php

declare(strict_types=1);

namespace App\Manager;

use App\Entity\Comment;
use App\Entity\Project;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CommentManager extends AbstractManager
{
    protected Generator $faker;

    public function __construct(
        protected EntityManagerInterface $manager,
        protected FormFactoryInterface $formFactory,
        protected TokenStorageInterface $storage,
        protected CommentRepository $repository,
        protected ProjectManager $projectManager,
    ) {
        parent::__construct($manager, $formFactory, $storage);
        $this->faker = Factory::create();
    }

    /**
     * @return Project[]
     */
    public function fetchAll(
        int $projectId,
        int $page = 1,
        int $items = 6,
    ): array {
        return $this->repository
            ->fetchAllByproject(
                $projectId,
                $page,
                $items,
            )
            ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     *
     * @return mixed[]
     */
    public function count(
        int $projectId
    ): array {
        return $this->repository
            ->lengthByProject(
                $projectId,
            )
            ;
    }

    public function fetch(int $id): Comment
    {
        try {
            $comment = $this->repository->fetch($id);
        } catch (NonUniqueResultException $exception) {
            throw new NotFoundHttpException();
        }

        if (!$comment) {
            throw new NotFoundHttpException();
        }

        return $comment;
    }

    public function like(int $id): Comment
    {
        $comment = $this->fetch($id);
        $comment->setPositivePoint($comment->getPositivePoint() + 1);

        $this->manager->persist($comment);
        $this->manager->flush();

        return $comment;
    }

    public function unlike(int $id): Comment
    {
        $comment = $this->fetch($id);

        $comment->setNegativePoint($comment->getNegativePoint() + 1);

        $this->manager->persist($comment);
        $this->manager->flush();

        return $comment;
    }

    /**
     * @param array<mixed> $data
     *
     * @throws Exception
     *
     * @return FormInterface|Comment
     */
    public function update(array $data, int $id)
    {
        $comment = $this->fetch($id);

        if($comment->getCreatedBy()->getId() !== $this->user->getId()){
            throw new UnauthorizedHttpException('You can\'t update this project', code: 401);
        }

        $form = $this->formFactory->create(CommentType::class, $comment);
        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var Comment $comment */
        $comment = $form->getData();

        $this->manager->flush();

        return $comment;
    }

    /**
     * @param array<mixed> $data
     *
     * @throws Exception
     *
     * @return FormInterface|Comment
     */
    public function create(int $projectId, array $data)
    {
        $currentUser = $this->user;
        $project = $this->projectManager->fetch($projectId);

        $comment = new Comment();
        $comment->setCreatedBy($currentUser);
        $comment->setProject($project);

        $form = $this->formFactory->create(CommentType::class, $comment);
        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var Comment $comment */
        $comment = $form->getData();

        $this->manager->persist($comment);
        $this->manager->flush();

        return $comment;
    }

    public function delete(int $id): void
    {
        $comment = $this->fetch($id);
        if($comment->getCreatedBy()->getId() !== $this->user->getId()){
            throw new UnauthorizedHttpException('You can\'t delete this project', code: 401);
        }
        $this->manager->remove($comment);
        $this->manager->flush();
    }
}
