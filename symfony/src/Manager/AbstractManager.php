<?php

declare(strict_types=1);

namespace App\Manager;

use App\Entity\Company;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

abstract class AbstractManager
{
    protected EntityManagerInterface $manager;

    protected FormFactoryInterface $formFactory;

    protected ?User $user = null;

    protected ?Company $company = null;

    public function __construct(EntityManagerInterface $manager, FormFactoryInterface $formFactory, TokenStorageInterface $storage)
    {
        $this->manager = $manager;
        $this->formFactory = $formFactory;

        if ($token = $storage->getToken()) {
            /** @var User $user */
            $user = $token->getUser();
            $this->user = $user;

            if ($company = $user->getCompany()) {
                $this->company = $company;
            }
        }
    }
}
