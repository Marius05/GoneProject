<?php

declare(strict_types=1);

namespace App\Manager;

use App\Entity\Project;
use App\Entity\User;
use App\Form\ProjectType;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProjectManager extends AbstractManager
{
    protected Generator $faker;

    public function __construct(
        protected EntityManagerInterface $manager,
        protected FormFactoryInterface $formFactory,
        protected TokenStorageInterface $storage,
        protected ProjectRepository $repository,
    ) {
        parent::__construct($manager, $formFactory, $storage);
        $this->faker = Factory::create();
    }

    /**
     * @return Project[]
     */
    public function fetchAll(
        int $page = 1,
        int $items = 6,
        ?string $sortBy = null,
        ?string $sortDirection = null,
        ?string $search = null
    ): array {
        return $this->repository
            ->fetchAll(
                $page,
                $items,
                $sortBy,
                $sortDirection,
                $search
            )
            ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     *
     * @return mixed[]
     */
    public function count(
        ?string $sortBy = null,
        ?string $sortDirection = null,
        ?string $status = null,
        ?string $search = null
    ): array {
        return $this->repository
            ->length(
                $sortBy,
                $sortDirection,
                $status,
                $search
            )
            ;
    }

    public function fetch(int $id): Project
    {
        $currentUser = $this->user;
        try {
            $project = $this->repository->fetch($id);
        } catch (NonUniqueResultException $exception) {
            throw new NotFoundHttpException();
        }

        if (!$project) {
            throw new NotFoundHttpException();
        }

        $project = $this->updateLiked($project, $currentUser);

        return $project;
    }

    public function like(int $id): Project
    {
        $currentUser = $this->user;
        try {
            $project = $this->repository->fetch($id);
        } catch (NonUniqueResultException $exception) {
            throw new NotFoundHttpException();
        }

        if (!$project) {
            throw new NotFoundHttpException();
        }

        if ($currentUser->getLikedProjects()->contains($project)){
            $currentUser->removeLikedProject($project);
        } else {
            $currentUser->addLikedProject($project);
        }

        $this->manager->persist($currentUser);
        $this->manager->flush();

        $project = $this->updateLiked($project, $currentUser);

        return $project;
    }

    /**
     * @param array<mixed> $data
     *
     * @throws Exception
     *
     * @return FormInterface|Project
     */
    public function update(array $data, int $id)
    {
        $project = $this->fetch($id);

        if($project->getUser()->getId() !== $this->user->getId()){
            throw new UnauthorizedHttpException('You can\'t update this project', code: 401);
        }

        $form = $this->formFactory->create(ProjectType::class, $project);
        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var Project $project */
        $project = $form->getData();

        $this->manager->flush();

        $project = $this->updateLiked($project, $this->user);

        return $project;
    }

    /**
     * @param array<mixed> $data
     *
     * @throws Exception
     *
     * @return FormInterface|Project
     */
    public function create(array $data)
    {
        $currentUser = $this->user;

        $project = new Project();
        $project->setUser($currentUser);

        $form = $this->formFactory->create(ProjectType::class, $project);
        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var Project $project */
        $project = $form->getData();

        $this->manager->persist($project);
        $this->manager->flush();

        return $project;
    }

    public function delete(int $id): void
    {
        $project = $this->fetch($id);
        if($project->getUser()->getId() !== $this->user->getId()){
            throw new UnauthorizedHttpException('You can\'t delete this project', code: 401);
        }
        $this->manager->remove($project);
        $this->manager->flush();
    }

    private function updateLiked(Project $project, User $user): Project
    {
        $project->setLiked($user->getLikedProjects()->contains($project));

        return $project;
    }
}
