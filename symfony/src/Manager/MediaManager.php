<?php

declare(strict_types=1);

namespace App\Manager;

use App\Entity\Media;
use App\Repository\MediaRepository;
use App\Service\MediaService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use League\Flysystem\FileNotFoundException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MediaManager extends AbstractManager
{
    protected MediaRepository $repository;

    protected MediaService $mediaService;

    public function __construct(
        EntityManagerInterface $manager,
        FormFactoryInterface $formFactory,
        TokenStorageInterface $storage,
        MediaService $mediaService,
        MediaRepository $repository
    ) {
        parent::__construct($manager, $formFactory, $storage);
        $this->repository = $repository;
        $this->mediaService = $mediaService;
    }

    public function fetch(string $uid): Media
    {
        try {
            $media = $this->repository->fetch($uid);
        } catch (NonUniqueResultException $exception) {
            throw new NotFoundHttpException();
        }

        if (!$media) {
            throw new NotFoundHttpException();
        }

        return $media;
    }

    /**
     * @throws Exception
     */
    public function create(UploadedFile $file, string $visibility = 'public'): Media
    {
        $filename = $this->mediaService->write($file);

        $media = (new Media())
            ->setVisibility($visibility)
            ->setExtension($file->guessExtension())
            ->setOriginalName($file->getClientOriginalName())
            ->setName($filename)
        ;

        if ($this->user) {
            $media->setCreatedBy($this->user);
        }

        $this->manager->persist($media);
        $this->manager->flush();

        return $media;
    }

    /**
     * @throws FileNotFoundException
     *
     * @return string[]
     */
    public function display(string $uid): array
    {
        $media = $this->fetch($uid);

        $file = $this->mediaService
            ->read($media->getName())
        ;

        $mime = $this->mediaService->extensionToMime(
            $media->getExtension()
        );

        return [$file, $mime];
    }

    /**
     * @throws FileNotFoundException
     */
    public function delete(string $uid): void
    {
        $media = $this->fetch($uid);
        $this->mediaService->delete($media->getName());
        $this->manager->remove($media);
        $this->manager->flush();
    }
}
