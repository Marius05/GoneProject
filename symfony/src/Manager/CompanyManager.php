<?php

declare(strict_types=1);

namespace App\Manager;

use App\Entity\Company;
use App\Form\CompanyEditType;
use App\Form\CompanyType;
use App\Repository\CompanyRepository;
use App\Service\MediaService;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CompanyManager extends AbstractManager
{
    protected CompanyRepository $repository;

    protected MediaService $mediaService;

    public function __construct(
        EntityManagerInterface $manager,
        FormFactoryInterface $formFactory,
        TokenStorageInterface $storage,
        CompanyRepository $repository,
        MediaService $mediaService
    ) {
        parent::__construct($manager, $formFactory, $storage);
        $this->mediaService = $mediaService;
        $this->repository = $repository;
    }

    public function fetch(int $id): Company
    {
        try {
            $company = $this->repository->fetch($id);
        } catch (NonUniqueResultException $exception) {
            throw new NotFoundHttpException();
        }

        if (!$company) {
            throw new NotFoundHttpException();
        }

        return $company;
    }

    /**
     * @param array<mixed> $data
     *
     * @return Company|FormInterface
     */
    public function create(array $data)
    {
        $form = $this->formFactory->create(CompanyType::class, new Company());
        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var Company $company */
        $company = $form->getData();
        $company
            ->setSlug(
                (new Slugify())->slugify($company->getName())
            )
        ;

        $this->manager->persist($company);
        $this->manager->flush();

        $company->setSlug(
            sprintf('%s-%s', $company->getId(), $company->getSlug())
        );
        $this->manager->flush();

        return $company;
    }

    /**
     * @param array<mixed> $data
     *
     * @return Company|FormInterface
     */
    public function update(array $data, int $id)
    {
        $company = $this->fetch($id);
        $form = $this->formFactory->create(CompanyEditType::class, $company);
        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        /** @var Company $company */
        $company = $form->getData();
        $this->manager->flush();

        return $company;
    }
}
