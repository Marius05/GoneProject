<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ProjectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @OA\Schema(title="Project")
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{
    /**
     * @OA\Property()
     * @Serializer\Groups({"project"})
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @OA\Property(
     *     maximum="255"
     * )
     *
     * @Serializer\Groups({"project"})
     *
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name = null;

    /**
     * @OA\Property(
     *     maximum="255"
     * )
     *
     * @Serializer\Groups({"project"})
     *
     * @ORM\Column(type="string", length=255)
     */
    private ?string $resume = null;

    /**
     * @OA\Property(
     *     maximum="255"
     * )
     *
     * @Serializer\Groups({"project"})
     *
     * @ORM\Column(type="string", length=255)
     */
    private ?string $tags = null;

    /**
     * @OA\Property(
     *     maximum="255"
     * )
     *
     * @Serializer\Groups({"project"})
     *
     * @ORM\Column(type="text")
     */
    private ?string $description = null;

    /**
     * @Serializer\Groups({"project"})
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Media", mappedBy="project", cascade={"persist", "remove"})
     */
    private Collection $documents;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private ?\DateTime $createdAt;

    /**
     * @Serializer\Groups({"project"})
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="createdProjects")
     */
    private User $user;

    /**
     * @Serializer\Groups({"project"})
     */
    private ?bool $liked = false;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->documents = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Project
     */
    public function setName(?string $name): Project
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return Project
     */
    public function setEmail(?string $email): Project
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     * @param string|null $website
     * @return Project
     */
    public function setWebsite(?string $website): Project
    {
        $this->website = $website;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Project
     */
    public function setDescription(?string $description): Project
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getActivity(): ?string
    {
        return $this->activity;
    }

    /**
     * @param string|null $activity
     * @return Project
     */
    public function setActivity(?string $activity): Project
    {
        $this->activity = $activity;
        return $this;
    }

    /**
     * @return Media|null
     */
    public function getLogo(): ?Media
    {
        return $this->logo;
    }

    /**
     * @param Media|null $logo
     * @return Project
     */
    public function setLogo(?Media $logo): Project
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime|null $createdAt
     * @return Project
     */
    public function setCreatedAt(?\DateTime $createdAt): Project
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function setUser(User $user) : self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string|null
     */
    public function getResume(): ?string
    {
        return $this->resume;
    }

    /**
     * @param string|null $resume
     * @return Project
     */
    public function setResume(?string $resume): Project
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTags(): ?string
    {
        return $this->tags;
    }

    /**
     * @param string|null $tags
     * @return Project
     */
    public function setTags(?string $tags): Project
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    /**
     * @param Collection $documents
     * @return Project
     */
    public function setDocuments(Collection $documents): Project
    {
        $this->documents = $documents;

        return $this;
    }

    public function getLiked(): ?bool
    {
        return $this->liked;
    }

    public function setLiked(?bool $liked): self
    {
        $this->liked = $liked;

        return $this;
    }
}
