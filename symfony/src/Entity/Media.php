<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\MediaRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(title="Media", description="Fichier")
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=MediaRepository::class)
 */
class Media
{
    public const TYPE_PICTURE = 'picture';

    /**
     * @OA\Property()
     * @Serializer\Groups({"media"})
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @OA\Property()
     *
     * @ORM\Column(type="string", length=255)
     */
    protected ?string $name = null;

    /**
     * @OA\Property(
     *     enum={"identity", "assurance", "picture", "document", "bill", "qrcode"}
     * )
     *
     * @Serializer\Groups({"media"})
     *
     * @ORM\Column(type="string", length=255)
     */
    protected ?string $type = null;

    /**
     * @OA\Property()
     *
     * @ORM\Column(type="string", length=20)
     */
    protected ?string $extension = null;

    /**
     * @OA\Property()
     *
     * @ORM\Column(type="string", length=10)
     */
    protected ?string $visibility = null;

    /**
     * @OA\Property()
     *
     * @Serializer\Groups({"media"})
     *
     * @ORM\Column(type="string", length=255)
     */
    protected ?string $originalName = null;

    /**
     * @OA\Property()
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected ?int $height = null;

    /**
     * @OA\Property()
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected ?int $width = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="documents")
     * @ORM\JoinColumn(nullable=true)
     */
    protected ?Project $project = null;

    /**
     * @Serializer\Groups({"media", "user_info", "search", "creator"})
     */
    protected ?string $path = null;

    public function __construct()
    {
        $this->type = self::TYPE_PICTURE;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function setExtension(string $extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    public function getOriginalName(): ?string
    {
        return $this->originalName;
    }

    public function setOriginalName(string $originalName): self
    {
        $this->originalName = $originalName;

        return $this;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(?int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(?int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getTransaction(): ?Transaction
    {
        return $this->transaction;
    }

    public function setTransaction(?Transaction $transaction): self
    {
        $this->transaction = $transaction;

        return $this;
    }

    public function getCashOut(): ?CashOut
    {
        return $this->cashOut;
    }

    public function setCashOut(?CashOut $cashOut): self
    {
        $this->cashOut = $cashOut;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getVisibility(): ?string
    {
        return $this->visibility;
    }

    public function setVisibility(?string $visibility): self
    {
        $this->visibility = $visibility;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }
}
