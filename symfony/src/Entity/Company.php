<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(title="Company")
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 */
class Company
{
    /**
     * @OA\Property()
     * @Serializer\Groups({"company"})
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @OA\Property(
     *     maximum="255"
     * )
     *
     * @Serializer\Groups({"company"})
     *
     * @ORM\Column(type="string", length=255)
     */
    private ?string $name = null;

    /**
     * @OA\Property(
     *     maximum="255"
     * )
     *
     * @Serializer\Groups({"company"})
     *
     * @ORM\Column(type="string", length=255)
     */
    private ?string $email = null;

    /**
     * @OA\Property(
     *     maximum="255"
     * )
     *
     * @Serializer\Groups({"company"})
     *
     * @ORM\Column(type="string", length=255)
     */
    private ?string $website = null;

    /**
     * @OA\Property(
     *     maximum="255"
     * )
     *
     * @Serializer\Groups({"company"})
     *
     * @ORM\Column(type="text")
     */
    private ?string $description = null;

    /**
     * @OA\Property()
     *
     * @Serializer\Groups({"company"})
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $activity = null;

    /**
     * @OA\Property(ref="#/components/schemas/Media")
     * @ORM\OneToOne(targetEntity="App\Entity\Media")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Media $logo = null;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private ?\DateTime $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Company
     */
    public function setName(?string $name): Company
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return Company
     */
    public function setEmail(?string $email): Company
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     * @param string|null $website
     * @return Company
     */
    public function setWebsite(?string $website): Company
    {
        $this->website = $website;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Company
     */
    public function setDescription(?string $description): Company
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getActivity(): ?string
    {
        return $this->activity;
    }

    /**
     * @param string|null $activity
     * @return Company
     */
    public function setActivity(?string $activity): Company
    {
        $this->activity = $activity;
        return $this;
    }

    /**
     * @return Media|null
     */
    public function getLogo(): ?Media
    {
        return $this->logo;
    }

    /**
     * @param Media|null $logo
     * @return Company
     */
    public function setLogo(?Media $logo): Company
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime|null $createdAt
     * @return Company
     */
    public function setCreatedAt(?\DateTime $createdAt): Company
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}
