<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(title="Comment")
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 */
class Comment
{
    /**
     * @OA\Property()
     * @Serializer\Groups({"comment"})
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @Serializer\Groups({"comment"})
     *
     * @ORM\Column(type="text")
     */
    private ?string $comment = null;

    /**
     * @Serializer\Groups({"comment"})
     *
     * @ORM\Column(type="integer")
     */
    private ?int $positivePoint = 0;

    /**
     * @Serializer\Groups({"comment"})
     *
     * @ORM\Column(type="integer")
     */
    private ?int $negativePoint = 0;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private ?\DateTime $createdAt;

    /**
     * @Serializer\Groups({"comment"})
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private ?User $createdBy = null;

    /**
     * @Serializer\Groups({"comment"})
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     */
    private ?Project $project = null;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     * @return Comment
     */
    public function setComment(?string $comment): Comment
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPositivePoint(): ?int
    {
        return $this->positivePoint;
    }

    /**
     * @param int|null $positivePoint
     * @return Comment
     */
    public function setPositivePoint(?int $positivePoint): Comment
    {
        $this->positivePoint = $positivePoint;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getNegativePoint(): ?int
    {
        return $this->negativePoint;
    }

    /**
     * @param int|null $negativePoint
     * @return Comment
     */
    public function setNegativePoint(?int $negativePoint): Comment
    {
        $this->negativePoint = $negativePoint;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime|null $createdAt
     * @return Comment
     */
    public function setCreatedAt(?\DateTime $createdAt): Comment
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User|null $createdBy
     * @return Comment
     */
    public function setCreatedBy(?User $createdBy): Comment
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return Project|null
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project|null $project
     * @return Comment
     */
    public function setProject(?Project $project): Comment
    {
        $this->project = $project;
        return $this;
    }
}
