<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CompanyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(title="Team")
 * @ORM\Entity(repositoryClass=TeamRepository::class)
 */
class Team
{
    /**
     * @OA\Property()
     * @Serializer\Groups({"team"})
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;


    /**
     * @Serializer\Groups({"team"})
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="teams", cascade={"persist", "remove"})
     */
    private Collection $users;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Project")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Project $project = null;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getUsers(): ArrayCollection|Collection
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection|Collection $users
     * @return Team
     */
    public function setUsers(ArrayCollection|Collection $users): Team
    {
        $this->users = $users;
        return $this;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }

        return $this;
    }

    /**
     * @return Project|null
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @param Project|null $project
     * @return Team
     */
    public function setProject(?Project $project): Team
    {
        $this->project = $project;
        return $this;
    }
}
