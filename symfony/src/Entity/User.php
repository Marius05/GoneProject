<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @OA\Schema(title="User")
 *
 * @UniqueEntity("email")
 *
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    public const TYPE_USER = 'user';

    public const TYPE_OWNER = 'owner';

    public const TYPE_ADMIN = 'admin';

    /**
     * @OA\Property()
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @Serializer\Groups({"user", "user_info"})
     */
    private ?int $id = null;

    /**
     * @OA\Property()
     *
     * @Serializer\Groups({"user", "user_info"})
     *
     * @Assert\Email()
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $password = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $salt = null;

    /**
     * @OA\Property()
     *
     * @Serializer\Groups({"user", "user_info"})
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $firstName = null;

    /**
     * @OA\Property()
     *
     * @Serializer\Groups({"user", "user_info"})
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $lastName = null;


    /**
     * @OA\Property()
     *
     * @Serializer\Groups({"user", "user_info"})
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $phone = null;

    /**
     * @OA\Property()
     *
     * @Serializer\Groups({"user", "user_info"})
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $job = null;

    /**
     * @OA\Property()
     *
     * @Serializer\Groups({"user"})
     *
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private ?string $type = self::TYPE_USER;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $requestPassword = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $requestPasswordAt = null;

    /**
     * @var string[]
     *
     * @ORM\Column(type="array")
     */
    private array $roles = [];

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private ?\DateTime $deletedAt;

    /**
     * @ORM\Column(name="createdAt", type="datetime", nullable=true)
     */
    private ?\DateTime $createdAt;

    /**
     * @Serializer\Groups({"user"})
     *
     * @OA\Property(
     *     ref="#/components/schemas/Company"
     * )
     * @ORM\ManyToOne(targetEntity="App\Entity\Company")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Company $company = null;

    /**
     * @Serializer\Groups({"user_info"})
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Media $avatar = null;

    /**
     * @Serializer\Groups({"user_infos"})
     * @ORM\ManyToMany(targetEntity="Project")
     * @ORM\JoinTable(name="users_projects",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id")}
     *      )
     */
    private Collection $likedProjects;

    /**
     * @Serializer\Groups({"user_infos"})
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Project", mappedBy="user", cascade={"persist", "remove"})
     */
    private Collection $createdProjects;

    /**
     * @Serializer\Groups({"user_infos"})
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Team", mappedBy="users", cascade={"persist", "remove"})
     */
    private Collection $teams;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="user", cascade={"persist", "remove"})
     */
    private Collection $comments;

    /**
     * @OA\Property(
     *     maximum="255"
     * )
     *
     * @Serializer\Groups({"user_infos", "user"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description = null;

    private ?string $plainPassword = null;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->comments = new ArrayCollection();
        $this->teams = new ArrayCollection();
        $this->createdProjects = new ArrayCollection();
        $this->likedProjects = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function addRole(string $role): self
    {
        if (!in_array($role, $this->roles)){
            $this->roles[] = $role;
        }

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return User
     */
    public function setEmail(?string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     * @return User
     */
    public function setFirstName(?string $firstName): User
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     * @return User
     */
    public function setLastName(?string $lastName): User
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return User
     */
    public function setPhone(?string $phone): User
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getJob(): ?string
    {
        return $this->job;
    }

    /**
     * @param string|null $job
     * @return User
     */
    public function setJob(?string $job): User
    {
        $this->job = $job;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return User
     */
    public function setType(?string $type): User
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRequestPassword(): ?string
    {
        return $this->requestPassword;
    }

    /**
     * @param string|null $requestPassword
     * @return User
     */
    public function setRequestPassword(?string $requestPassword): User
    {
        $this->requestPassword = $requestPassword;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getRequestPasswordAt(): ?\DateTime
    {
        return $this->requestPasswordAt;
    }

    /**
     * @param \DateTime|null $requestPasswordAt
     * @return User
     */
    public function setRequestPasswordAt(?\DateTime $requestPasswordAt): User
    {
        $this->requestPasswordAt = $requestPasswordAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime|null $deletedAt
     * @return User
     */
    public function setDeletedAt(?\DateTime $deletedAt): User
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime|null $createdAt
     * @return User
     */
    public function setCreatedAt(?\DateTime $createdAt): User
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Company|null $company
     * @return User
     */
    public function setCompany(?Company $company): User
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return Media|null
     */
    public function getAvatar(): ?Media
    {
        return $this->avatar;
    }

    /**
     * @param Media|null $avatar
     * @return User
     */
    public function setAvatar(?Media $avatar): User
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string|null $plainPassword
     * @return User
     */
    public function setPlainPassword(?string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getLikedProjects(): ArrayCollection|Collection
    {
        return $this->likedProjects;
    }

    public function addLikedProject(Project $project): self
    {
        if (!$this->likedProjects->contains($project)){
            $this->likedProjects->add($project);
        }

        return $this;
    }

    public function removeLikedProject(Project $project): self
    {
        if ($this->likedProjects->contains($project)){
            $this->likedProjects->removeElement($project);
        }

        return $this;
    }

    /**
     * @param ArrayCollection|Collection $likedProjects
     * @return User
     */
    public function setLikedProjects(ArrayCollection|Collection $likedProjects): User
    {
        $this->likedProjects = $likedProjects;
        return $this;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getCreatedProjects(): ArrayCollection|Collection
    {
        return $this->createdProjects;
    }

    /**
     * @param ArrayCollection|Collection $createdProjects
     * @return User
     */
    public function setCreatedProjects(ArrayCollection|Collection $createdProjects): User
    {
        $this->createdProjects = $createdProjects;
        return $this;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getTeams(): ArrayCollection|Collection
    {
        return $this->teams;
    }

    /**
     * @param ArrayCollection|Collection $teams
     * @return User
     */
    public function setTeams(ArrayCollection|Collection $teams): User
    {
        $this->teams = $teams;
        return $this;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getComments(): ArrayCollection|Collection
    {
        return $this->comments;
    }

    /**
     * @param ArrayCollection|Collection $comments
     * @return User
     */
    public function setComments(ArrayCollection|Collection $comments): User
    {
        $this->comments = $comments;
        return $this;
    }

   /**
    * @return string|null
    */
   public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return User
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
