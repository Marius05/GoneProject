<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method null|Project find($id, $lockMode = null, $lockVersion = null)
 * @method null|Project findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function fetch(int $id): ?Project
    {
        return $this->createQueryBuilder('p')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function fetchAll(
        int $page = 1,
        int $items = 6,
        ?string $sortBy = null,
        ?string $sortDirection = 'ASC',
        ?string $search = null
        ): array
    {
        $builder = $this->createQueryBuilder('p')
            ->setMaxResults($items)
            ->setFirstResult(($page - 1) * $items)
            ;

        $this->filters($builder, $sortBy, $sortDirection, $search);

        return $builder->getQuery()->getResult();
    }

    private function filters(
        QueryBuilder $builder,
        ?string $sortBy = null,
        ?string $sortDirection = 'ASC',
        ?string $search = null
    ): QueryBuilder {
        if ($sortBy) {
            $builder->orderBy($sortBy, $sortDirection);
        }

        if ($search) {
            $builder->andWhere(
                $builder->expr()->orX(
                    $builder->expr()->like('p.name', ':search'),
                    $builder->expr()->like('p.resume', ':search'),
                )
            )->setParameter('search', '%'.$search.'%');
        }

        return $builder;
    }
}
