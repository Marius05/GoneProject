<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Company;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

abstract class AbstractRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, string $class)
    {
        parent::__construct($registry, $class);
    }

    protected function whereOwner(QueryBuilder $builder, ?User $user, ?Company $company): void
    {
        $alias = $builder->getRootAliases()[0];

        if ($company) {
            $builder
                ->andWhere(sprintf('%s.company = :company', $alias))
                ->setParameter('company', $company)
            ;
        }

        if ($user) {
            $builder
                ->andWhere(sprintf('%s.createdBy = :user', $alias))
                ->setParameter('user', $user)
            ;
        }
    }
}
