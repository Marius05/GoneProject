<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method null|User find($id, $lockMode = null, $lockVersion = null)
 * @method null|User findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function fetch(int $id): ?User
    {
        return $this->createQueryBuilder('u')
            ->addSelect('a, c')
            ->leftJoin('u.avatar', 'a')
            ->leftJoin('u.company', 'c')
            ->andWhere('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function fetchByExternalId(string $external): ?User
    {
        return $this->createQueryBuilder('u')
            ->addSelect('a, d, c, ba')
            ->leftJoin('u.avatar', 'a')
            ->leftJoin('u.documents', 'd')
            ->leftJoin('u.company', 'c')
            ->leftJoin('u.bankAccount', 'ba')
            ->andWhere('u.paymentExternalId = :id')
            ->setParameter('id', $external)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function fetchByEmail(string $email, ?bool $anon = null): ?User
    {
        $builder = $this->createQueryBuilder('u')
            ->addSelect('a, c')
            ->leftJoin('u.avatar', 'a')
            ->leftJoin('u.company', 'c')
            ->andWhere('u.email = :email')
            ->setParameter('email', $email)
        ;

        if ($anon) {
            $builder->andWhere('u.isAnon = :anon')
                ->setParameter('anon', $anon)
            ;
        }

        return $builder
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function fetchByToken(string $token): ?User
    {
        return $this->createQueryBuilder('u')
            ->addSelect('a')
            ->leftJoin('u.avatar', 'a')
            ->andWhere('u.requestPassword = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    /**
     * @return User[]
     */
    public function fetchAll(
        int $page = 1,
        int $items = 6,
        ?string $sortBy = null,
        ?string $sortDirection = 'ASC',
        ?string $status = null,
        ?string $search = null
    ): array {
        $builder = $this->createQueryBuilder('u')
            ->addSelect('c')
            ->leftJoin('u.company', 'c')
            ->setMaxResults($items)
            ->setFirstResult(($page - 1) * $items)
            ;

        $this->filters($builder, $sortBy, $sortDirection, $status, $search);

        return $builder->getQuery()
            ->getResult()
            ;
    }

    /**
     * @throws NoResultException
     * @throws NonUniqueResultException
     *
     * @return mixed[]
     */
    public function length(
        ?string $sortBy = null,
        ?string $sortDirection = 'ASC',
        ?string $status = null,
        ?string $search = null
    ): array {
        $builder = $this->createQueryBuilder('u')
            ->select('COUNT(u.id) as count')
            ->leftJoin('u.company', 'c')
            ->where('u.deletedAt is null')
        ;

        $this->filters($builder, $sortBy, $sortDirection, $status, $search);

        return $builder->getQuery()
            ->getSingleResult()
            ;
    }

    /**
     * @return User[]
     */
    public function search(string $me, string $search): array
    {
        $builder = $this->createQueryBuilder('u');

        $builder
            ->where(
                $builder->expr()->orX(
                    $builder->expr()->like('u.email', ':search'),
                    $builder->expr()->like('u.lastName', ':search'),
                )
            )
            ->andWhere('u.email != :me')
            ->andWhere('u.isAnon = :false')
            ->andWhere('u.roles LIKE :role')
            ->setParameter('false', false)
            ->setParameter('me', $me)
            ->setParameter('search', '%'.$search.'%')
            ->setParameter('role', '%ROLE_TENANT%')
            ->setMaxResults(5)
        ;

        return $builder->getQuery()
            ->getResult()
        ;
    }

    private function filters(
        QueryBuilder $builder,
        ?string $sortBy = null,
        ?string $sortDirection = 'ASC',
        ?string $status = null,
        ?string $search = null
    ): QueryBuilder {
        if ($sortBy) {
            $builder->orderBy($sortBy, $sortDirection);
        }

        if ($status && 'null' !== $status) {
            $builder->andWhere('u.type = :type')
                ->setParameter('type', $status)
            ;
        }

        if ($search) {
            $builder->andWhere(
                $builder->expr()->orX(
                    $builder->expr()->like('u.firstName', ':search'),
                    $builder->expr()->like('u.lastName', ':search'),
                    $builder->expr()->like('u.email', ':search'),
                    $builder->expr()->like('u.internal', ':search'),
                    $builder->expr()->like('c.name', ':search')
                )
            )->setParameter('search', '%'.$search.'%');
        }

        return $builder;
    }
}
