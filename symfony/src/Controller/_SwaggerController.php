<?php

declare(strict_types=1);

namespace App\Controller;

use OpenApi;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @OA\Info(
 *   title="Tyllt API",
 *   version="1.0.0",
 *   contact={
 *     "email": "contact@feelity.fr"
 *   }
 * )
 * @OA\OpenApi(
 *     security={
 *         {"api_key": {}}
 *     }
 * )
 * @OA\Server(
 *     url="http://localhost",
 *     description="Development"
 * )
 * @OA\Server(
 *     url="https://uat.api.tyllt.fr",
 *     description="UAT"
 * )
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     in="header",
 *     securityScheme="api_key",
 *     name="Authorization"
 * )
 */
class _SwaggerController extends AbstractController
{
    protected string $projectDirectory;

    public function __construct(string $directory)
    {
        $this->projectDirectory = $directory;
    }

    /**
     * @Route(
     *     name="swagger_generate",
     *     path="/swagger/json"
     * )
     */
    public function generateAction(): Response
    {
        $json = OpenApi\scan(
            sprintf('%s/src', $this->projectDirectory)
        );

        return new Response($json->toJson(), 200, [
            'CONTENT-TYPE' => 'application/json',
        ]);
    }

    /**
     * @Route(
     *     path="/swagger/ui",
     *     name="swagger_ui"
     * )
     */
    public function uiAction(): Response
    {
        return $this->render('Swagger/swagger.html.twig');
    }
}
