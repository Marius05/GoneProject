<?php

declare(strict_types=1);

namespace App\Controller;

use App\Manager\MediaManager;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use League\Flysystem\FileNotFoundException;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Throwable;

/**
 * Class MediaController.
 */
class MediaController extends AbstractFOSRestController
{
    public function __construct(
        protected MediaManager $manager,
    ) {
    }

    /**
     * @OA\Get(
     *     tags={"Media"},
     *     path="/medias/{uid}/display",
     *     summary="Display file",
     *     @OA\Parameter(name="uid", in="path"),
     *     @OA\Response(
     *         response=200,
     *         description="File"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     *
     * @Rest\Get(
     *     name="media_display",
     *     path="/medias/{uid}",
     * )
     *
     * @throws FileNotFoundException
     */
    public function displayAction(string $uid): Response
    {
        [$file, $mime] = $this->manager->display($uid);

        return new Response(
            $file,
            Response::HTTP_OK,
            [
                'Content-Type' => $mime,
                'Content-Disposition' => ResponseHeaderBag::DISPOSITION_INLINE,
            ]
        );
    }

    /**
     * @OA\Delete(
     *     tags={"Media"},
     *     path="/medias/{uid}/delete",
     *     summary="Delete an upload file",
     *     @OA\Parameter(name="uid", in="path"),
     *     @OA\Response(
     *         response=200,
     *         description="File"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     *
     * @Rest\Delete(
     *     name="media_delete",
     *     path="/medias/{uid}",
     * )
     *
     * @throws FileNotFoundException
     */
    public function deleteAction(string $uid): Response
    {
        $this->manager->delete($uid);

        return new Response(
            null,
            Response::HTTP_NO_CONTENT
        );
    }

    /**
     * @OA\Post(
     *     tags={"Media"},
     *     path="/medias",
     *     summary="Create a media",
     *     @OA\Response(
     *         response=201,
     *         description="File uploaded and media created"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected issue"
     *     )
     * )
     *
     * @Rest\Post(
     *     name="media_create",
     *     path="/medias"
     * )
     *
     * @Rest\FileParam(name="file")
     * @Rest\RequestParam(
     *     name="visibility",
     *     default="public",
     *     requirements="(public|private)",
     *     strict=true
     * )
     * @Rest\RequestParam(
     *     name="type",
     *     default="document",
     *     requirements="(identity|assurance|picture|document|bill)",
     *     strict=true
     * )
     *
     * @Rest\View(serializerGroups={"media"})
     *
     * @throws Exception
     */
    public function createAction(ParamFetcherInterface $fetcher): View
    {
        return $this->view(
            $this->manager->create(
                $fetcher->get('file'),
                $fetcher->get('visibility'),
            ),
            Response::HTTP_CREATED
        );
    }

    /**
     * @Rest\Get(
     *     name="cashOut_zip",
     *     path="zip/cash-out/{uid}"
     * )
     *
     * @throws Exception
     */
    public function zipCashOutAction(string $uid): Response
    {
        [$file, $filename] = $this->cashOutManager->zipCashout($uid);

        try {
            return $this->file($file, $filename);
        } catch (Throwable $e) {
            throw new \Exception('Aucun fichier trouvé');
        }
    }

    /**
     * @Rest\Get(
     *     name="check_zip",
     *     path="zip/check/{id}"
     * )
     *
     * @throws Exception
     */
    public function zipCheckAction(string $id): Response
    {
        [$file, $filename] = $this->checkManager->zip($id);

        try {
            return $this->file($file, $filename);
        } catch (Throwable $e) {
            throw new \Exception('Aucun fichier trouvé');
        }
    }

    /**
     * @Rest\Get(
     *     name="transaction_documents_zip",
     *     path="zip/transaction/{id}"
     * )
     *
     * @throws Exception
     */
    public function zipTransactionAction(string $id): Response
    {
        [$file, $filename] = $this->transactionManager->zip($id);

        try {
            return $this->file($file, $filename);
        } catch (Throwable $e) {
            throw new \Exception('Aucun fichier trouvé');
        }
    }
}
