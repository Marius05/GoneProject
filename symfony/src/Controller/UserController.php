<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Manager\UserManager;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use OpenApi\Annotations as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController.
 *
 * @Rest\Route("/v1/users")
 */
class UserController extends AbstractFOSRestController
{
    protected UserManager $manager;

    public function __construct(UserManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @OA\Get(
     *   path="/v1/users",
     *   summary="list users",
     *   tags={"User"},
     *   @OA\Parameter(
     *      name="page", in="query", required=false, @OA\Schema(type="integer", default="1")
     *   ),
     *   @OA\Parameter(
     *      name="items", in="query", required=false, @OA\Schema(type="integer", default="6")
     *   ),
     *   @OA\Parameter(
     *      name="sortBy", in="query", required=false, description="Sort by field (format: x.xxxx)", @OA\Schema(type="string")
     *   ),
     *   @OA\Parameter(
     *      name="sortDirection", in="query", required=false, @OA\Schema(type="string", enum={"ASC", "DESC"})
     *   ),
     *   @OA\Parameter(
     *      name="status", in="query", required=false, @OA\Schema(type="string")
     *   ),
     *   @OA\Parameter(
     *      name="search", in="query", required=false, @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="A list with users",
     *     @OA\JsonContent(
     *         type="array",
     *         @OA\Items(ref="#/components/schemas/User")
     *     ),
     *   ),
     *   @OA\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     *
     * @Rest\Get(
     *     path="",
     *     name="user_list"
     * )
     *
     * @Rest\QueryParam(name="page", nullable=true, default="1", requirements="\d+")
     * @Rest\QueryParam(name="items", nullable=true, default="6", requirements="\d+")
     *
     * @Rest\QueryParam(name="sortBy", nullable=true, requirements="^[a-z]\.[a-zA-Z]+")
     * @Rest\QueryParam(name="sortDirection", nullable=true, requirements="(ASC|DESC)")
     *
     * @Rest\QueryParam(name="status", nullable=true, requirements="^[a-z_]+$")
     * @Rest\QueryParam(name="search", nullable=true)
     *
     * @Rest\View(serializerGroups={"user"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function listAction(ParamFetcherInterface $fetcher): View
    {
        return $this->view(
            $this->manager->fetchAll(
                (int) $fetcher->get('page', true),
                (int) $fetcher->get('items', true),
                $fetcher->get('sortBy', true),
                $fetcher->get('sortDirection', true),
                $fetcher->get('status', true),
                $fetcher->get('search', true),
            )
        );
    }

    /**
     * @OA\Get(
     *   path="/v1/users/count",
     *   summary="count users",
     *   tags={"User"},
     *   @OA\Parameter(
     *      name="page", in="query", required=false, @OA\Schema(type="integer", default="1")
     *   ),
     *   @OA\Parameter(
     *      name="items", in="query", required=false, @OA\Schema(type="integer", default="6")
     *   ),
     *   @OA\Parameter(
     *      name="sortBy", in="query", required=false, description="Sort by field (format: x.xxxx)", @OA\Schema(type="string")
     *   ),
     *   @OA\Parameter(
     *      name="sortDirection", in="query", required=false, @OA\Schema(type="string", enum={"ASC", "DESC"})
     *   ),
     *   @OA\Parameter(
     *      name="status", in="query", required=false, @OA\Schema(type="string", enum={"in_progress", "litigation", "finish"})
     *   ),
     *   @OA\Parameter(
     *      name="search", in="query", required=false, @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="A user count",
     *     @OA\JsonContent(
     *         type="array",
     *         @OA\Items(ref="#/components/schemas/User")
     *     ),
     *   ),
     *   @OA\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     *
     * @Rest\Get(
     *     path="/count",
     *     name="user_count"
     * )
     *
     * @Rest\QueryParam(name="sortBy", nullable=true, requirements="^[a-z]\.[a-zA-Z]+")
     * @Rest\QueryParam(name="sortDirection", nullable=true, requirements="(ASC|DESC)")
     *
     * @Rest\QueryParam(name="status", nullable=true, requirements="^[a-z_]+$")
     * @Rest\QueryParam(name="search", nullable=true)
     *
     * @Rest\View()
     */
    public function countAction(ParamFetcherInterface $fetcher): View
    {
        return $this->view(
            $this->manager->count(
                $fetcher->get('sortBy', true),
                $fetcher->get('sortDirection', true),
                $fetcher->get('status', true),
                $fetcher->get('search', true)
            )
        );
    }

    /**
     * @OA\Get(
     *     tags={"User"},
     *     path="/v1/users/{id}",
     *     summary="Get user",
     *     @OA\Parameter(name="id", in="path"),
     *     @OA\Response(
     *         response=200,
     *         description="User details",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     *
     * @Rest\Get(
     *     name="user_read",
     *     path="/{id}",
     *     requirements={"id":"\d+"}
     * )
     *
     * @Rest\View(serializerGroups={"user", "user_info", "company", "media"})
     */
    public function readAction(int $id): View
    {
        return $this->view(
            $this->manager->fetch($id)
        );
    }

    /**
     * @OA\Get(
     *     tags={"User"},
     *     path="/v1/users/me",
     *     summary="Get the current user",
     *     @OA\Response(
     *         response=200,
     *         description="User details",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     )
     * )
     *
     * @Rest\Get(
     *     name="user_me",
     *     path="/me"
     * )
     *
     * @Rest\View(serializerGroups={"user", "document", "user_info", "media", "company", "date", "bank"})
     */
    public function meAction(): View
    {
        /** @var User $user */
        $user = $this->getUser();

        return $this->view(
            $this->manager->fetch(
                $user->getId(),
                true
            )
        );
    }

    /**
     * @OA\Put(
     *     tags={"User"},
     *     path="/v1/users/{id}",
     *     summary="Update user",
     *     @OA\Parameter(name="id", in="path"),
     *     @OA\Response(
     *         response=200,
     *         description="User details",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\RequestBody(
     *         description="Data",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/UserType")
     *         )
     *     )
     * )
     *
     * @Rest\Put(
     *     name="user_update",
     *     path="/{id}",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\RequestParam(name="lastName", nullable=false)
     * @Rest\RequestParam(name="firstName", nullable=false)
     * @Rest\RequestParam(name="phone", nullable=true)
     * @Rest\RequestParam(name="description", nullable=true)
     *
     * @Rest\View(serializerGroups={"user", "user_info"})
     */
    public function updateAction(ParamFetcherInterface $fetcher, int $id): View
    {
        return $this->view(
            $this->manager->update(
                $fetcher->all(true),
                $id,
            )
        );
    }

    /**
     * @OA\Put(
     *     tags={"User"},
     *     path="/v1/users/me",
     *     summary="Update the current user",
     *     @OA\Response(
     *         response=200,
     *         description="User details",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\RequestBody(
     *         description="Data",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/UserType")
     *         )
     *     )
     * )
     *
     * @Rest\Put(
     *     name="user_me_update",
     *     path="/me"
     * )
     *
     * @Rest\RequestParam(name="lastName", nullable=false)
     * @Rest\RequestParam(name="firstName", nullable=false)
     * @Rest\RequestParam(name="phone", nullable=true)
     * @Rest\RequestParam(name="description", nullable=true)
     * @Rest\RequestParam(name="avatar", nullable=true)
     *
     * @Rest\View(serializerGroups={"user", "user_info"})
     *
     * @throws Exception
     */
    public function updateMeAction(ParamFetcherInterface $fetcher): View
    {
        return $this->view(
            $this->manager->update(
                $fetcher->all(true),
            )
        );
    }

    /**
     * @OA\Put(
     *     tags={"User"},
     *     path="/v1/users/password",
     *     summary="Update the password of the current user",
     *     @OA\Response(
     *         response=200,
     *         description="User details",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\RequestBody(
     *         description="Data",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/UserType")
     *         )
     *     )
     * )
     *
     * @Rest\Put(
     *     name="user_password_update",
     *     path="/password"
     * )
     *
     * @Rest\RequestParam(name="password", nullable=false)
     * @Rest\RequestParam(name="plainPassword", nullable=false)
     *
     * @Rest\View(serializerGroups={"user", "user_info"})
     */
    public function updatePasswordAction(ParamFetcherInterface $fetcher): View
    {
        return $this->view(
            $this->manager->changePasswordWithCheck(
                $fetcher->all(true),
            )
        );
    }

    /**
     * @OA\Delete(
     *   path="/v1/users/{id}",
     *   summary="Delete an user",
     *   tags={"Bond"},
     *   @OA\Parameter(
     *      name="id", in="path", required=true
     *   ),
     *   @OA\Response(
     *     response=204,
     *     description="user delete with success",
     *   ),
     *   @OA\Response(
     *     response=404,
     *     description="Not Found"
     *   ),
     *   @OA\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     *
     * @Rest\Delete(
     *     path="/{id}",
     *     name="user_delete",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\View(serializerGroups={"bond"})
     */
    public function deleteAction(int $id): View
    {
        $this->manager->delete($id);

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @OA\Delete(
     *   path="/v1/users/{id}/anonymize",
     *   summary="anonymize an user",
     *   tags={"Bond"},
     *   @OA\Parameter(
     *      name="id", in="path", required=true
     *   ),
     *   @OA\Response(
     *     response=204,
     *     description="user anonymize with success",
     *   ),
     *   @OA\Response(
     *     response=404,
     *     description="Not Found"
     *   ),
     *   @OA\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     *
     * @Rest\Delete(
     *     path="/{id}/anonymize",
     *     name="user_anonymize",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\View(serializerGroups={"bond"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function anonymizeAction(int $id): View
    {
        $this->manager->anonymize($id);

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }
}
