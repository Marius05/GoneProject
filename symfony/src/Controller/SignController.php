<?php

declare(strict_types=1);

namespace App\Controller;

use App\Manager\UserManager;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SignController.
 */
class SignController extends AbstractFOSRestController
{
    /**
     * @OA\Post(
     *     tags={"Authentication"},
     *     path="/login_check",
     *     summary="Get JWT",
     *     @OA\Response(
     *         response=200,
     *         description="Token"
     *     ),
     *     @OA\RequestBody(
     *         description="Data",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(property="email", type="string"),
     *                 @OA\Property(property="password", type="string")
     *             )
     *         )
     *     )
     * )
     */
    protected UserManager $manager;

    public function __construct(UserManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Rest\Post(
     *     name="sign_user",
     *     path="/sign"
     * )
     *
     * @Rest\RequestParam(name="email", nullable=false)
     * @Rest\RequestParam(name="plainPassword", nullable=false)
     * @Rest\RequestParam(name="type", nullable=false, requirements="(user|owner)")
     * @Rest\RequestParam(name="lastName", nullable=false)
     * @Rest\RequestParam(name="firstName", nullable=false)
     * @Rest\RequestParam(name="phone", nullable=true)
     * @Rest\RequestParam(name="description", nullable=true)
     *
     * @Rest\View(serializerGroups={"user"})
     *
     * @throws Exception
     */
    public function userAction(JWTTokenManagerInterface $JWTManager, ParamFetcherInterface $fetcher): View
    {
        $user = $this->manager->sign(
            $fetcher->all(true)
        );

        dump($user);

        return $this->view(
            ['token' => $JWTManager->create($user)]
        );
    }

    /**
     * @Rest\Post(
     *     name="request_password_create",
     *     path="/request-password"
     * )
     *
     * @Rest\RequestParam(name="email", nullable=false)
     */
    public function requestPasswordAction(ParamFetcherInterface $fetcher): View
    {
        $this->manager->requestPassword(
            $fetcher->get('email')
        );

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Get(
     *     name="request_password",
     *     path="/request-password/{token}"
     * )
     */
    public function requestAction(string $token): View
    {
        $this->manager->fetchByRequestPassword($token);

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Post(
     *     name="change_password",
     *     path="/change-password/{token}"
     * )
     *
     * @Rest\RequestParam(name="plainPassword", nullable=false)
     *
     * @Rest\View()
     */
    public function changePasswordAction(ParamFetcherInterface $fetcher, string $token): View
    {
        return $this->view(
            $this->manager->changePasswordToken(
                $fetcher->all(),
                $token
            ),
            Response::HTTP_OK
        );
    }
}
