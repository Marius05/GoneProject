<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Manager\ProjectManager;
use App\Manager\UserManager;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use OpenApi\Annotations as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProjectController.
 *
 * @Rest\Route("/v1/projects")
 */
class ProjectController extends AbstractFOSRestController
{
    protected ProjectManager $manager;

    public function __construct(ProjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Rest\Get(
     *     path="",
     *     name="project_list"
     * )
     *
     * @Rest\QueryParam(name="page", nullable=true, default="1", requirements="\d+")
     * @Rest\QueryParam(name="items", nullable=true, default="6", requirements="\d+")
     *
     * @Rest\QueryParam(name="sortBy", nullable=true, requirements="^[a-z]\.[a-zA-Z]+")
     * @Rest\QueryParam(name="sortDirection", nullable=true, requirements="(ASC|DESC)")
     *
     * @Rest\QueryParam(name="search", nullable=true)
     *
     * @Rest\View(serializerGroups={"project"})
     */
    public function listAction(ParamFetcherInterface $fetcher): View
    {
        return $this->view(
            $this->manager->fetchAll(
                (int) $fetcher->get('page', true),
                (int) $fetcher->get('items', true),
                $fetcher->get('sortBy', true),
                $fetcher->get('sortDirection', true),
                $fetcher->get('search', true),
            )
        );
    }

    /**
     * @Rest\Get(
     *     path="/count",
     *     name="project_count"
     * )
     *
     * @Rest\QueryParam(name="sortBy", nullable=true, requirements="^[a-z]\.[a-zA-Z]+")
     * @Rest\QueryParam(name="sortDirection", nullable=true, requirements="(ASC|DESC)")
     *
     * @Rest\QueryParam(name="status", nullable=true, requirements="^[a-z_]+$")
     * @Rest\QueryParam(name="search", nullable=true)
     *
     * @Rest\View()
     */
    public function countAction(ParamFetcherInterface $fetcher): View
    {
        return $this->view(
            $this->manager->count(
                $fetcher->get('sortBy', true),
                $fetcher->get('sortDirection', true),
                $fetcher->get('status', true),
                $fetcher->get('search', true)
            )
        );
    }

    /**
     * @Rest\Get(
     *     name="project_read",
     *     path="/{id}",
     *     requirements={"id":"\d+"}
     * )
     *
     * @Rest\View(serializerGroups={"project", "user_info", "company", "media"})
     */
    public function readAction(int $id): View
    {
        return $this->view(
            $this->manager->fetch($id)
        );
    }

    /**
     * @Rest\Post(
     *     name="project_create",
     *     path="",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\RequestParam(name="name", nullable=false)
     * @Rest\RequestParam(name="resume", nullable=false)
     * @Rest\RequestParam(name="tags", nullable=true)
     * @Rest\RequestParam(name="description", nullable=true)
     * @Rest\RequestParam(name="documents", nullable=true)
     *
     * @Rest\View(serializerGroups={"project", "user_info"})
     */
    public function createAction(ParamFetcherInterface $fetcher): View
    {
        return $this->view(
            $this->manager->create(
                $fetcher->all(true),
            )
        );
    }

    /**
     * @Rest\Put(
     *     name="project_update",
     *     path="/{id}",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\RequestParam(name="name", nullable=false)
     * @Rest\RequestParam(name="resume", nullable=false)
     * @Rest\RequestParam(name="tags", nullable=true)
     * @Rest\RequestParam(name="description", nullable=true)
     * @Rest\RequestParam(name="documents", nullable=true)
     *
     * @Rest\View(serializerGroups={"project", "user_info"})
     */
    public function updateAction(ParamFetcherInterface $fetcher, int $id): View
    {
        return $this->view(
            $this->manager->update(
                $fetcher->all(true),
                $id,
            )
        );
    }

    /**
     * @Rest\Put(
     *     name="project_like",
     *     path="/{id}/like",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\View(serializerGroups={"project", "user_info"})
     */
    public function likeAction(ParamFetcherInterface $fetcher, int $id): View
    {
        return $this->view(
            $this->manager->like(
                $id,
            )
        );
    }

    /**
     * @Rest\Delete(
     *     path="/{id}",
     *     name="project_delete",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\View(serializerGroups={"project"})
     */
    public function deleteAction(int $id): View
    {
        $this->manager->delete($id);

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }
}
