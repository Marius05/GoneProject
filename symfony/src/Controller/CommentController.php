<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Manager\CommentManager;
use App\Manager\ProjectManager;
use App\Manager\UserManager;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use OpenApi\Annotations as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CommentController.
 *
 * @Rest\Route("/v1/comments")
 */
class CommentController extends AbstractFOSRestController
{
    protected CommentManager $manager;

    public function __construct(CommentManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Rest\Get(
     *     path="/{projectId}",
     *     name="comment_list"
     * )
     *
     * @Rest\QueryParam(name="page", nullable=true, default="1", requirements="\d+")
     * @Rest\QueryParam(name="items", nullable=true, default="6", requirements="\d+")
     *
     * @Rest\View(serializerGroups={"comment", "user_info"})
     */
    public function listAction(ParamFetcherInterface $fetcher, int $projectId): View
    {
        return $this->view(
            $this->manager->fetchAll(
                $projectId,
                (int) $fetcher->get('page', true),
                (int) $fetcher->get('items', true),
            )
        );
    }

    /**
     * @Rest\Get(
     *     path="/{projectId}/count",
     *     name="comment_count"
     * )
     *
     * @Rest\View()
     */
    public function countAction(int $projectId): View
    {
        return $this->view(
            $this->manager->count(
                $projectId
            )
        );
    }

    /**
     * @Rest\Post(
     *     name="comment_create",
     *     path="/{projectId}",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\RequestParam(name="comment", nullable=false)
     *
     * * @Rest\View(serializerGroups={"comment", "user_info"})
     */
    public function createAction(ParamFetcherInterface $fetcher, int $projectId): View
    {
        return $this->view(
            $this->manager->create(
                $projectId,
                $fetcher->all(true),
            )
        );
    }

    /**
     * @Rest\Put(
     *     name="comment_update",
     *     path="/{id}",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\RequestParam(name="comment", nullable=false)
     *
     * @Rest\View(serializerGroups={"comment", "user_info"})
     */
    public function updateAction(ParamFetcherInterface $fetcher, int $id): View
    {
        return $this->view(
            $this->manager->update(
                $fetcher->all(true),
                $id
            )
        );
    }

    /**
     * @Rest\Put(
     *     name="comment_like",
     *     path="/{id}/like",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\View(serializerGroups={"comment", "user_info"})
     */
    public function likeAction(int $id): View
    {
        return $this->view(
            $this->manager->like(
                $id,
            )
        );
    }

    /**
     * @Rest\Put(
     *     name="comment_unlike",
     *     path="/{id}/unlike",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\View(serializerGroups={"comment", "user_info"})
     */
    public function unlikeAction(int $id): View
    {
        return $this->view(
            $this->manager->unlike(
                $id,
            )
        );
    }

    /**
     * @Rest\Delete(
     *     path="/{id}",
     *     name="comment_delete",
     *     requirements={"id": "\d+"}
     * )
     *
     * @Rest\View(serializerGroups={"project"})
     */
    public function deleteAction(int $id): View
    {
        $this->manager->delete($id);

        return $this->view(null, Response::HTTP_NO_CONTENT);
    }
}
