<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailService
{
    protected MJMLService $service;

    protected MailerInterface $mailer;

    protected string $from;

    protected string $shareDNS;

    protected ?Request $request;

    public function __construct(
        MailerInterface $mailer,
        MJMLService $service,
        RequestStack $requestStack,
        string $shareDNS,
        string $from
    ) {
        $this->from = $from;
        $this->mailer = $mailer;
        $this->service = $service;
        $this->shareDNS = $shareDNS;
        $this->request = $requestStack->getMasterRequest();
    }

    /**
     * @param mixed[] $args
     */
    public function prepare(string $to, string $subject, string $view, array $args = []): Email
    {
        $args = array_merge($args, [
            'dns' => $this->request ? $this->request->getSchemeAndHttpHost() : $this->shareDNS,
        ]);

        return (new Email())
            ->to($to)
            ->subject($subject)
            ->from($this->from)
            ->html(
                $this->service->render($view, $args)
            )
        ;
    }

    public function send(Email $email): bool
    {
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            return false;
        }

        return true;
    }
}
