<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Process\Process;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class MJMLService
{
    protected string $binary;

    protected string $node;

    protected Environment $twig;

    public function __construct(Environment $twig, string $mjmlBinary, string $nodeBinary)
    {
        $this->twig = $twig;
        $this->node = $nodeBinary;
        $this->binary = $mjmlBinary;
    }

    public function getVersion(): string
    {
        $command = [$this->node, $this->binary, '--version'];

        $process = new Process($command);
        $process->mustRun();

        preg_match('#\d\.\d+\.\d+#', $process->getOutput(), $match);

        return $match[0] ?? 'unknown';
    }

    /**
     * @param mixed[] $args
     */
    public function render(string $template, array $args = []): string
    {
        try {
            $view = $this->twig->render($template, $args);
        } catch (LoaderError | RuntimeError | SyntaxError $e) {
            return '';
        }

        $command = [$this->node, $this->binary, '-i', '-s', '--config.minify', 'true'];

        $process = new Process($command);
        $process->setInput($view);
        $process->mustRun();

        return $process->getOutput();
    }
}
