<?php

declare(strict_types=1);

namespace App\Service;

use Exception;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\RouterInterface;

class MediaService
{
    protected const MIMES_PICTURE = [
        'image/png',
        'image/jpeg',
        'image/gif',
        'application/pdf',
    ];

    protected string $minioEndpoint;

    protected RouterInterface $router;

    protected FilesystemInterface $storage;

    public function __construct(
        RouterInterface $router,
        FilesystemInterface $defaultStorage
    ) {
        $this->router = $router;
        $this->storage = $defaultStorage;
    }

    public function createFolder(string $name): bool
    {
        return $this->storage->createDir($name);
    }

    /**
     * @throws FileNotFoundException
     */
    public function read(string $filename): string
    {
        $file = $this->storage->read(
            sprintf('%s/%s', $this->getFolderHash($filename), $filename),
        );

        if (false === $file) {
            throw new FileNotFoundException('can not read the file');
        }

        return $file;
    }

    /**
     * @throws Exception
     */
    public function write(UploadedFile $file): string
    {
        if (!\in_array($file->getMimeType(), self::MIMES_PICTURE, true)) {
            throw new BadRequestHttpException('Mime not accepted');
        }

        return $this->writeWithString(
            file_get_contents($file->getRealPath())
        );
    }

    public function writeWithString(string $content): string
    {
        $filename = uniqid(md5((string) time()), true);

        try {
            $writer = $this->storage->write(
                sprintf('%s/%s', $this->getFolderHash($filename), $filename),
                $content,
            );

            if (!$writer) {
                throw new UploadException();
            }
        } catch (FileExistsException $exception) {
            throw new UploadException();
        }

        return $filename;
    }

    /**
     * @throws FileNotFoundException
     */
    public function delete(string $filename): bool
    {
        $path = sprintf('%s/%s', $this->getFolderHash($filename), $filename);

        return $this->storage->delete($path);
    }

    public function extensionToMime(string $extension): string
    {
        switch ($extension) {
            case 'jpeg':
            case 'jpg':
                return 'image/jpeg';
            case 'pdf':
                return 'application/pdf';
            case 'png':
                return 'image/png';
            default:
                return '';
        }
    }

    public function path(string $filename): string
    {
        return sprintf('%s/%s', $this->getFolderHash($filename), $filename);
    }

    private function getFolderHash(string $filename): string
    {
        return implode('/', \array_slice(
            str_split($filename, 2),
            0,
            2
        ));
    }
}
