<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Bond;
use App\Entity\CashOut;
use App\Entity\Company;
use App\Entity\Media;
use App\Entity\Product;
use App\Entity\Property;
use App\Entity\Ticket;
use App\Entity\Transaction;
use App\Entity\User;
use ICanBoogie\Inflector;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionProperty;

/**
 * Class EntitiesTest.
 */
class EntitiesTest extends TestCase
{
    private Inflector $inflector;

    public function testSetter(): void
    {
        $this->inflector = Inflector::get();

        $this->check(Company::class);
        $this->check(User::class);
        $this->check(Bond::class);
        $this->check(Media::class);
        $this->check(Transaction::class);
        $this->check(Product::class);
        $this->check(CashOut::class);
        $this->check(Ticket::class);
        $this->check(Property::class);
    }

    /**
     * @param $class
     */
    private function check($class): void
    {
        $reflectionClass = new ReflectionClass($class);

        $suitablesReturnSetter = ['self'];

        foreach ($reflectionClass->getInterfaceNames() as $interfaceName) {
            $name = explode('/', $interfaceName);
            $suitablesReturnSetter[] = $name[\count($name) - 1];
        }

        $properties = $reflectionClass->getProperties();
        $methods = $reflectionClass->getMethods();

        foreach ($properties as $property) {
            /** @var ReflectionProperty */
            $camelCases = $this->inflector->camelize($property->getName());

            if (!$property->getDocComment()) {
                continue;
            }

            if (preg_match_all('/OneToMany/', $property->getDocComment())) {
                $singularize = $this->inflector->singularize($property->getName());

                $this->assertTrue(
                    $reflectionClass->hasMethod('add'.ucfirst($singularize)),
                    sprintf('Add %s->%s missing', $class, $property->getName())
                );

                $this->assertTrue(
                    $reflectionClass->hasMethod('remove'.ucfirst($singularize)),
                    sprintf('Remove %s->%s missing', $class, $property->getName())
                );
            } else {
                if ('id' !== $property->getName()) {
                    $this->assertTrue(
                        $reflectionClass->hasMethod('set'.$camelCases),
                        sprintf('Setter %s->%s missing', $class, $property->getName())
                    );
                }
            }

            if ($property->getType()->getName() === 'bool') {
                $hasGetter = $reflectionClass->hasMethod('get'.$camelCases);

                if (!$hasGetter) {
                    $hasGetter = $reflectionClass->hasMethod('is'.$camelCases);
                }

                $this->assertTrue(
                    $hasGetter,
                    sprintf('Getter %s->%s missing', $class, $property->getName())
                );
            } else {
                $this->assertTrue(
                    $reflectionClass->hasMethod('get'.$camelCases),
                    sprintf('Getter %s->%s missing', $class, $property->getName())
                );
            }

            $this->assertFalse($property->isPublic(), 'Entity\'s property has to be private or protected');
        }

        foreach ($methods as $method) {
            if (preg_match('/set/', $method->name)) {
                $this->assertTrue(
                    \in_array($method->getReturnType()->getName(), $suitablesReturnSetter, true),
                    $class.'->'.$method->name.' Please execute php bin/console make:entity --regenerate --overwrite'
                );
            }
        }
    }
}
