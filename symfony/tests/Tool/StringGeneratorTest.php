<?php

declare(strict_types=1);

namespace App\Tests\Tool;

use App\Tool\StringGenerator;
use PHPUnit\Framework\TestCase;

/**
 * Class StringGeneratorTest.
 */
class StringGeneratorTest extends TestCase
{
    public function testGenerate(): void
    {
        $this->assertNotSame(
            StringGenerator::generate(),
            StringGenerator::generate()
        );

        $this->assertSame(100, \strlen(StringGenerator::generate(100)));
    }
}
