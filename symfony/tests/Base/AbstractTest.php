<?php

namespace App\Tests\Base;

use App\DataFixtures\UserFixtures;
use App\Kernel;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;
use Symfony\Component\Console\Application;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class AbstractTest.
 */
class AbstractTest extends WebTestCase
{
    /**
     * @var ContainerInterface
     */
    protected static $container;

    protected static Application $application;

    protected static EntityManagerInterface $manager;

    protected AbstractBrowser $client;

    protected Generator $faker;

    protected string $token = '';

    public static function setUpBeforeClass(): void
    {
        $kernel = new Kernel('test', true);
        $kernel->boot();

        self::$container = $kernel->getContainer();

        $manager = self::$container->get('doctrine.orm.entity_manager');
        if ($manager instanceof EntityManagerInterface) {
            self::$manager = $manager;
        }
    }

    protected function tearDown(): void
    {
        self::$manager->clear();
    }

    protected function initialize(): void
    {
        $this->faker = Factory::create();
        $this->client = static::createClient();
    }

    protected function login(string $username = UserFixtures::OWNER_EMAIL, string $password = 'owner'): void
    {
        $this->post('/login_check', [], json_encode(
            [
                'username' => $username,
                'password' => $password,
            ]
        ));

        $response = $this->getResponseContent(true);
        $this->token = $response['token'];
    }

    protected function get(string $uri, array $params = [], ?string $content = null, array $files = [], array $headers = []): Crawler
    {
        $headers = $this->addAuthorizationHeader($headers);

        return $this->client->request('GET', $uri, $params, $files, $headers, $content);
    }

    protected function post(string $uri, array $params = [], ?string $content = null, array $files = [], array $headers = []): Crawler
    {
        $headers = $this->addAuthorizationHeader($headers);

        return $this->client->request('POST', $uri, $params, $files, $headers, $content);
    }

    protected function put(string $uri, array $params = [], ?string $content = null, array $files = [], array $headers = []): Crawler
    {
        $headers = $this->addAuthorizationHeader($headers);

        return $this->client->request('PUT', $uri, $params, $files, $headers, $content);
    }

    protected function patch(string $uri, array $params = [], ?string $content = null, array $files = [], array $headers = []): Crawler
    {
        $headers = $this->addAuthorizationHeader($headers);

        return $this->client->request('PATCH', $uri, $params, $files, $headers, $content);
    }

    protected function delete(string $uri, array $params = [], ?string $content = null, array $files = [], array $headers = []): Crawler
    {
        $headers = $this->addAuthorizationHeader($headers);

        return $this->client->request('DELETE', $uri, $params, $files, $headers, $content);
    }

    /**
     * @param bool $json
     *
     * @return mixed|object
     */
    protected function getResponseContent($json = false)
    {
        if ($json) {
            return json_decode($this->client->getResponse()->getContent(), true);
        }

        return $this->client->getResponse()->getContent();
    }

    protected function getStatusCode(): int
    {
        return $this->client->getResponse()->getStatusCode();
    }

    private function addAuthorizationHeader(array $headers): array
    {
        if ('' === $this->token) {
            return $headers;
        }

        $authorization = [
            'HTTP_Authorization' => 'Bearer '.$this->token,
        ];

        return array_merge($authorization, $headers);
    }
}
