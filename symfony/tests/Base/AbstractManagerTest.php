<?php

declare(strict_types=1);

namespace App\Tests\Base;

use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Repository\UserRepository;
use Faker\Factory;
use Faker\Generator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class AbstractManagerTest.
 */
class AbstractManagerTest extends WebTestCase
{
    protected Generator $faker;

    protected ?User $currentUser = null;

    protected function init(): void
    {
        $this->faker = Factory::create();
        self::bootKernel();
    }

    protected function mockRequest(): void
    {
        $request = new Request();

        self::$container->get('request_stack')->push($request);
    }

    protected function login(): void
    {
        $session = self::$container->get('session');
        $security = self::$container->get('security.token_storage');

        $this->currentUser = self::$container
            ->get(UserRepository::class)
            ->findOneByEmail(UserFixtures::OWNER_EMAIL)
        ;

        $token = new UsernamePasswordToken($this->currentUser, null, 'main', $this->currentUser->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $security->setToken($token);
    }

    protected function debugForm(FormInterface $form): void
    {
        foreach ($form->getErrors(true, true) as $error) {
            dump($error->getOrigin()->getName());
        }

        die;
    }
}
