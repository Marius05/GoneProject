<?php

declare(strict_types=1);

namespace App\Tests\Manager;

use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Manager\UserManager;
use App\Tests\Base\AbstractManagerTest;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserManagerTest.
 */
class UserManagerTest extends AbstractManagerTest
{
    protected UserManager $manager;

    protected function setUp(): void
    {
        $this->init();
        $this->login();
        $this->mockRequest();
        $this->loadManager();
    }

    public function testFetchAll(): void
    {
        $users = $this->manager->fetchAll();
        $this->assertContainsOnlyInstancesOf(User::class, $users);
    }

    public function testSign(): int
    {
        $json = [
            'email' => $this->faker->email,
            'plainPassword' => [
                'first' => 'coucou',
                'second' => 'coucou',
            ],
        ];

        $user = $this->manager->sign($json);
        $this->assertInstanceOf(User::class, $user);

        return $user->getId();
    }

    public function testSignFailed(): void
    {
        $json = [
            'email' => 'owner@feelity.fr',
            'plainPassword' => [
                'first' => 'coucou',
                'second' => 'coucou',
            ],
        ];

        $form = $this->manager->sign($json);
        $this->assertInstanceOf(FormInterface::class, $form);

        foreach ($form->getErrors(true) as $error) {
            $this->assertSame('This value is already used.', $error->getMessage());
        }
    }

    /**
     * @depends testSign
     */
    public function testUpdate(int $id): void
    {
        $json = [
            'civility' => 'mr',
            'firstName' => $this->faker->firstName,
            'lastName' => $this->faker->lastName,
            'phone' => $this->faker->e164PhoneNumber,
            'nationality' => 'FR',
            'country' => 'FR',
            'birthAt' => '05/05/1985',
            'birthCity' => $this->faker->city,
            'birthCountry' => $this->faker->countryISOAlpha3,
            'street' => $this->faker->streetAddress,
            'postalCode' => $this->faker->postcode,
            'city' => $this->faker->city,
            'documents' => [],
        ];

        $user = $this->manager->update($json, $id);
        $this->assertInstanceOf(User::class, $user);
        $this->assertSame($json['lastName'], $user->getLastName());
        $this->assertSame($json['firstName'], $user->getFirstName());
    }

    /**
     * @depends testSign
     */
    public function testFetch(int $id): void
    {
        $user = $this->manager->fetch($id);
        $this->assertInstanceOf(User::class, $user);
    }

    public function testFetchFailed(): void
    {
        try {
            $this->manager->fetch(99999);
        } catch (NotFoundHttpException $exception) {
            $this->assertSame(404, $exception->getStatusCode());
        }
    }

    public function testVerifyCode(): void
    {
        $this->manager->verifyCode($this->currentUser->getEmailCode());
        $this->assertTrue(true);
    }

    public function testVerifyCodeFailed(): void
    {
        try {
            $this->manager->verifyCode('AAAA');
        } catch (\Exception $exception) {
            $this->assertSame('Code not match', $exception->getMessage());
        }
    }

    /**
     * @depends testSign
     */
    public function testPassword(int $id): void
    {
        $user = $this->manager->fetch($id);
        $password = $user->getPassword();
        $this->manager->upgradePassword($user, 'coucou');
        $this->assertNotSame($password, $user->getPassword());
    }

    public function testRequestPassword(): string
    {
        $token = $this->manager->requestPassword(UserFixtures::OWNER_EMAIL);
        $this->assertEquals(200, strlen($token));

        return $token;
    }

    /**
     * @depends testRequestPassword
     */
    public function testFetchByRequestPassword(string $token): User
    {
        $user = $this->manager->fetchByRequestPassword($token);
        $this->assertInstanceOf(User::class, $user);

        return $user;
    }

    /**
     * @depends testFetchByRequestPassword
     */
    public function testChangePassword(User $user): void
    {
        $password = $this->currentUser->getPassword();

        $json = [
            'plainPassword' => [
                'first' => 'coucou',
                'second' => 'coucou'
            ]
        ];

        $user = $this->manager->changePasswordToken($json, $user->getRequestPassword());
        $this->assertInstanceOf(User::class, $user);

        $this->assertNotSame($password, $user->getPassword());
    }

    public function testBecome(): void
    {
        if (!in_array('ROLE_TENANT', $this->currentUser->getRoles())) {
            $user = $this->manager->become('tenant');
            $this->assertInstanceOf(User::class, $user);
            return;
        }

        $this->assertNull(
            $this->manager->become('tenant')
        );
    }

    public function testCreateAdmin(): int
    {
        $json = [
            'email' => $this->faker->email,
            'lastName' => $this->faker->lastName,
            'firstName' => $this->faker->firstName,
            'plainPassword' => [
                'first' => 'coucou',
                'second' => 'coucou',
            ],
        ];

        $user = $this->manager->createAdmin($json);
        $this->assertInstanceOf(User::class, $user);

        return $user->getId();
    }

    public function testCreateAdminFailed(): void
    {
        $email = $this->faker->email;
        $json = [
            'email' => $email,
            'lastName' => $this->faker->lastName,
            'firstName' => $this->faker->firstName,
            'plainPassword' => [
                'first' => 'coucou',
                'second' => 'coucou',
            ],
        ];

        $user = $this->manager->createAdmin($json);
        $this->assertInstanceOf(User::class, $user);

        $json = [
            'email' => $email,
            'lastName' => '',
            'firstName' => '',
            'plainPassword' => [
                'first' => 'coucou',
                'second' => 'couc',
            ],
        ];
        $form = $this->manager->createAdmin($json);
        $this->assertInstanceOf(FormInterface::class, $form);

        foreach ($form->getErrors(true, true) as $error) {
            switch ($error->getOrigin()->getName()) {
                case 'email':
                    $this->assertSame('This value is already used.', $error->getMessage());
                    break;
                case 'lastName':
                case 'firstName':
                    $this->assertSame('This value should not be blank.', $error->getMessage());
                    break;
                case 'plainPassword':
                    $this->assertSame('The password fields must match.', $error->getMessage());
                    break;
            }
        }
    }

    /**
     * @depends testCreateAdmin
     */
    public function testUpdateAdmin(int $id): int
    {
        $json = [
            'email' => $this->faker->email,
            'firstName' => $this->faker->firstName,
            'lastName' => $this->faker->lastName
        ];

        $user = $this->manager->updateAdmin($json, $id);
        $this->assertInstanceOf(User::class, $user);
        $this->assertSame($json['lastName'], $user->getLastName());
        $this->assertSame($json['firstName'], $user->getFirstName());

        return $user->getId();
    }

    /**
     * @depends testUpdateAdmin
     */
    public function testDeleteAdmin(int $id): void
    {
        $this->manager->delete($id);

        try {
            $this->manager->fetch(99999);
        } catch (NotFoundHttpException $exception) {
            $this->assertSame(404, $exception->getStatusCode());
        }
    }

    /**
     * @depends testSign
     */
    public function testDisabled(int $id): void
    {
        $user = $this->manager->disabled($id);
        $this->assertSame(false, $user->isEnabled());

        $user = $this->manager->disabled($id);
        $this->assertSame(true, $user->isEnabled());
    }

    /**
     * @depends testSign
     */
    public function testAnonymize(int $id): void
    {
        $user = $this->manager->fetch($id);
        $oldEmail = $user->getEmail();
        $oldFirstName = $user->getFirstName();
        $oldLastName = $user->getLastName();
        $oldPhone = $user->getPhone();
        $oldStreet = $user->getStreet();
        $this->manager->anonymize($id);

        $this->assertNotEquals($oldEmail, $user->getEmail());
        $this->assertNotEquals($oldFirstName, $user->getFirstName());
        $this->assertNotEquals($oldLastName, $user->getLastName());
        $this->assertNotEquals($oldPhone, $user->getPhone());
        $this->assertNotEquals($oldStreet, $user->getStreet());
    }

    protected function loadManager(): void
    {
        $manager = self::$container->get(UserManager::class);

        if ($manager instanceof UserManager) {
            $this->manager = $manager;
        }
    }
}
