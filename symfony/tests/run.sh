#!/bin/bash

php bin/console lexik:jwt:generate-keypair --skip-if-exists

php bin/console d:d:d --env=test --force --if-exists
php bin/console d:d:c --env=test
#php bin/console d:m:m -n --env=test
php bin/console d:s:u -f --env=test
php bin/console doctrine:fixtures:load --env=test --append

./vendor/bin/phpunit --coverage-text \
    --colors=never \
    --coverage-html=var/cover \
    --coverage-clover tests/coverage-clover.xml \
    --log-junit tests/unitests-results.xml
