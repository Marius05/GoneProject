<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Tests\Base\AbstractTest;

/**
 * Class SwaggerControllerTest.
 */
class SwaggerControllerTest extends AbstractTest
{
    protected function setUp(): void
    {
        $this->initialize();
    }

    public function testSwagger(): void
    {
        $this->get('/swagger/ui');
        $this->assertSame(200, $this->getStatusCode());
    }

    public function testSwaggerJSON(): void
    {
        $this->get('/swagger/json');
        $this->assertSame(200, $this->getStatusCode());
    }
}
