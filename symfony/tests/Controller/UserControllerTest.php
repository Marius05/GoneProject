<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Tests\Base\AbstractTest;

/**
 * Class UserControllerTest.
 */
class UserControllerTest extends AbstractTest
{
    protected function setUp(): void
    {
        $this->initialize();
        $this->login();
    }

    public function testUpdateMe(): void
    {
        $json = [
            'civility' => 'mr',
            'firstName' => $this->faker->firstName,
            'lastName' => $this->faker->lastName,
            'phone' => $this->faker->e164PhoneNumber,
            'nationality' => 'FR',
            'country' => 'FR',
            'birthAt' => '05/05/1985',
            'birthCountry' => 'FR',
            'street' => $this->faker->streetAddress,
            'postalCode' => $this->faker->postcode,
            'city' => $this->faker->city,
            'documents' => [],
        ];

        $this->put('/v1/users/me', $json);

        $response = $this->getResponseContent(true);

        $this->assertSame(200, $this->getStatusCode());
        $this->assertSame($response['firstName'], $json['firstName']);
        $this->assertSame($response['nationality'], $json['nationality']);
    }
}
