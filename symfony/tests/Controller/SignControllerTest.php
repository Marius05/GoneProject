<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\DataFixtures\UserFixtures;
use App\Entity\User;
use App\Tests\Base\AbstractTest;

/**
 * Class SignControllerTest.
 */
class SignControllerTest extends AbstractTest
{
    protected function setUp(): void
    {
        $this->initialize();
    }

    public function testSign(): void
    {
        $this->post('/sign', [
            'email' => $this->faker->email,
            'plainPassword' => [
                'first' => 'coucou',
                'second' => 'coucou',
            ],
        ]);

        $this->assertSame(201, $this->getStatusCode());
    }

    public function testCode(): void
    {
        $this->login();

        $user = self::$manager
            ->getRepository(User::class)
            ->findOneBy(['email' => UserFixtures::OWNER_EMAIL])
        ;

        $this->post('/v1/sign/code', [
            'code' => $user->getEmailCode(),
        ]);

        $this->assertSame(204, $this->getStatusCode());
    }
}
