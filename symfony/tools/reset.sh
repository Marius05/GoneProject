#!/bin/bash

php bin/console d:d:d --force --if-exists
php bin/console d:d:c
php bin/console d:m:m -n
php bin/console doctrine:fixtures:load --append
