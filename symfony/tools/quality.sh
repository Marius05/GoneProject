#!/bin/bash

php bin/console doctrine:schema:validate --skip-sync

./vendor/bin/ecs check src
./vendor/bin/phpstan analyse --level=6 --memory-limit=-1 src
