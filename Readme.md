GoneProject API
=========

API Rest **Symfony 5 LTS** and **Docker**.

[![PHPStan](https://camo.githubusercontent.com/7314d8f36d2bc7052b0f451e8816749270620e87/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f5048505374616e2d656e61626c65642d627269676874677265656e2e7376673f7374796c653d666c6174)]()
[![pipeline status](https://git.tools.feelity.fr/tyllt/backend/badges/develop/pipeline.svg)](https://git.tools.feelity.fr/tyllt/backend/-/commits/develop)
[![coverage report](https://git.tools.feelity.fr/tyllt/backend/badges/develop/coverage.svg)](https://git.tools.feelity.fr/tyllt/backend/-/commits/develop)

## Stack
- PHP 8.1
- NGINX 1.17-alpine
- MySQL 8
- REDIS 5-alpine
- Adminer

## Installation
```bash
cp .env.dist .env

#Edition des variables d'environnement
nano .env

#Lancement du projet
docker-compose up -d
```

## Development

```bash
#Se connecter au container
docker exec -ti symfony_dev bash
composer install

#Génère les clés RSA pour JWT (le passphrase est dans symfony/.env)
php bin/console lexik:jwt:generate-keypair --skip-if-exists

#Initialise la base et les fixtures
sh ./tools/reset.sh
```

## Commandes

Dans le container **symfony_dev**

```bash
stan # Lance l'analyse avec PHPStan
ecs # Fix le dossier src avec PHP-CS-FIXER
run-test # Lance les tests du projet
```

## Swagger

    http://localhost/swagger

